from sqlalchemy.ext.automap import automap_base
from sqlalchemy.orm import Session
from sqlalchemy import create_engine
import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
from matplotlib.colors import hex2color
import numpy as np

#From https://gist.github.com/ollieglass/f6ddd781eeae1d24e391265432297538
kelly_colors = ['#F2F3F4', '#222222', '#F3C300', '#875692', '#F38400', '#A1CAF1', '#BE0032', '#C2B280', '#848482', '#008856', '#E68FAC', '#0067A5', '#F99379', '#604E97', '#F6A600', '#B3446C', '#DCD300', '#882D17', '#8DB600', '#654522', '#E25822', '#2B3D26']
colors=[]
for c in kelly_colors:
    colors.append(hex2color(c))
# engine, threadperprobe db:
engine = create_engine("sqlite:///../Results/CFO_thrPerProbe.db")

# query everything
db = pd.read_sql_query("SELECT function, avg(totaltime) as time, np, threads FROM results GROUP BY Function, threads;", engine)

# serial engine for Nd=30 & optimal Np
serialEngine = create_engine("sqlite:///../Results/CFO_Final.db")
ser = pd.read_sql_query("SELECT function, avg(totaltime) as time FROM results WHERE Nd=30 AND threads=1 GROUP BY function, threads;", serialEngine)

index = db.index
for f in index:
    speedup = ser['time'][f] / db['time'][f]
    db['time'].set_value(f, speedup)

fig = plt.figure()


ax = fig.gca()

for f in index:
    ax.scatter(db['Np'][f], db['time'][f], s=200, c=colors[f], label=db['Function'][f])
ax.set_xlabel("Np")
ax.set_ylabel("Speedup")
ax.legend(loc='center left', bbox_to_anchor=(0.99, 0.5),prop={'size':10})  


# ax = db.plot.scatter(x='Np', y='time', c=colors , s=50 )
# ax.legend(colors)
plt.show()

'''
serial : CFO_Final.db
sqlite> select function, avg(totaltime) as time from results where Nd=30 and threads=1  group by function, threads;
F1|1.5393186
F10|13.789625
F11|13.802238
F12|13.792115
F13|17.973991
F14|13.778146
F15|6.0300465
F16|13.787658
F17|13.781438
F18|13.779258
F2|9.4492337
F20|13.773061
F21|21.543301
F22|1.5308777
F23|21.592874
F3|1.5360212
F4|6.0148623
F5|13.75551
F7|16.328489
F8|13.793939
F9|13.795581

>>> db speedup
   Function      time   Np  threads
0        F1  2.475156   60       60
1       F10  5.395299  180      180
2       F11  5.612232  180      180
3       F12  5.699968  180      180
4       F13  9.299053  120      120
5       F14  6.100258  180      180
6       F15  8.499388  120      120
7       F16  6.464400  180      180
8       F17  6.438061  180      180
9       F18  6.064483  180      180
10       F2  4.278829  120      120
11      F20  6.431966  180      180
12      F21  6.341537  180      180
13      F22  4.737872   60       60
14      F23  7.285866  120      120
15       F3  3.427166   60       60
16       F4  4.904963  120      120
17       F5  4.373711  180      180
18       F7  5.691096  120      120
19       F8  4.859485  180      180
20       F9  5.122646  180      180

F1|60|30|1.5393186|1
F10|180|30|13.789625|1
F11|180|30|13.802238|1
F12|180|30|13.792115|1
F13|120|30|17.973991|1
F14|180|30|13.778146|1
F15|120|30|6.0300465|1
F16|180|30|13.787658|1
F17|180|30|13.781438|1
F18|180|30|13.779258|1
F2|120|30|9.4492337|1
F20|180|30|13.773061|1
F21|180|30|21.543301|1
F22|60|30|1.5308777|1
F23|120|30|21.592874|1
F3|60|30|1.5360212|1
F4|120|30|6.0148623|1
F5|180|30|13.75551|1
F7|120|30|16.328489|1
F8|180|30|13.793939|1
F9|180|30|13.795581|1


'''
