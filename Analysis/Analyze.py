import sqlite3 as sql
# import pystaggrelite3 as pyAgg
import fileinput
import pandas as pd
import pylab as p
import numpy as np
import matplotlib.ticker as ticker
from scipy.interpolate import griddata
import glob
import matplotlib
from sqlalchemy import create_engine # database connection
from matplotlib.ticker import LinearLocator, FormatStrFormatter
from mpl_toolkits.mplot3d import axes3d
from matplotlib import *
from matplotlib.lines import Line2D
import os
matplotlib.style.use('ggplot')

from scipy import stats

# Pruners     = ['ACO', 'AIS', 'GA', 'PSO']
Systems     = ['RTS79', 'MRTS', 'RTS96']
# lineColors  = ["#A00000", "#00 A000", "#5060D0", "#F25900", "#BB00D4"]
# lineMarkers = ['+', 'o', 'x', '^', 'D']
# figureDir   = "..\\Figures\\"

def analyzeData(dbFile):
    print(os.getcwd())
    db = create_engine('sqlite:///' + os.path.dirname(os.path.abspath(__file__)) + "/" + dbFile)

    df = pd.read_sql_query('SELECT * FROM Samples', db)

    # ipdType     = df.groupby('ipdType').get_group('MCS')
    # cfoType     = ipdType.groupby('cfoType').get_group('MCS_OMP_Batch')
    # mcsPipeline = df.groupby('Sampler').get_group('MCS_OMP_Pipeline')

    # mcs         = mcs.groupby('System')
    # mcsBatch    = mcsBatch.groupby(['System', 'numThreads', 'BatchSize'])
    # mcsPipeline = mcsPipeline.groupby(['System', 'genThreads', 'classThreads'])


    # ###################################### Begin Batch Level Parallelism ######################################
    # # pLine = "{:s} {:2,d} {:>6,.0f} {:>7,.4f} +/- {:>6,.4f} {:>6,.4f} +/- {:>6,.4f} {:>6,.4f}"
    # # for system in ["RTS79", "MRTS", "RTS96"]:
    # #     sTime  = mcs.get_group(system)['TotalTime'].mean()
    # #     for name, group in mcsBatch:
    # #          if name[0] == system:
    # #             print pLine.format(name[0], name[1], name[2],
    # #                                 group['TotalTime'].mean(), group['TotalTime'].std(),
    # #                                 group['SamplerLOLP'].mean(), group['SamplerLOLP'].std(),
    # #                                 group['TotalTime'].mean()/sTime)
    # #     print ""

    # ###################################### Begin Batch Level Parallelism  Graphs ######################################
    # for system in ["RTS79", "MRTS", "RTS96"]:
    #     nThreads = np.array([name[1] for name, group in mcsBatch if name[0] == system], np.dtype('float64'))
    #     bSize    = np.array([name[2] for name, group in mcsBatch if name[0] == system], np.dtype('float64'))
    #     time     = np.array([group['TotalTime'].mean() for name, group in mcsBatch if name[0] == system], np.dtype('float64'))

    #     sTime    = mcs.get_group(system)['TotalTime'].mean()
    #     speedup  = np.array([sTime/x for x in time], np.dtype('float64'))
    #     eff      = speedup/nThreads

    #     surfacePlot(nThreads, bSize, time,    figureTitle="", xAxisTitle='# Threads', yAxisTitle='Batch Size', zAxisTitle='Time(s)',
    #                 saveToFile = system + "_Batch_Time.png")
    #     surfacePlot(nThreads, bSize, speedup, figureTitle="", xAxisTitle='# Threads', yAxisTitle='Batch Size', zAxisTitle='Speedup',
    #                 saveToFile = system + "_Batch_Speedup.png")
    #     surfacePlot(nThreads, bSize, eff,     figureTitle="", xAxisTitle='# Threads', yAxisTitle='Batch Size', zAxisTitle='Efficiency',
    #                 saveToFile = system + "_Batch_Eff.png")


    # ###################################### Begin Pipeline Level Parallelism ######################################

    # # #pLine = "{:s} {:2,d} {:2,d} {:>7,.4f} +/- {:>7,.4f} {:>7,.4f} +/- {:>7,.4f}"
    # # pLine = "{:s}, {:2,d}, {:2,d}, {:>7,.4f}, {:>7,.4f}, {:>7,.4f}, {:>7,.4f}"

    # # for system in ["RTS79"]:
    # #     sTime  = mcs.get_group(system)['TotalTime'].mean()

    # #     for name, group in mcsPipeline:
    # #         if name[0] == system:
    # #             print pLine.format(name[0], name[1], name[2],
    # #                                 group['TotalTime'].mean(), group['TotalTime'].std(),
    # #                                 group['SamplerLOLP'].mean(), group['SamplerLOLP'].std(),
    # #                                 group['TotalTime'].mean()/sTime)
    # #     print ""
    #             # print pLine.format(name[0], name[1], name[2],
    #             #            group['TotalTime'].mean(), group['TotalTime'].std(),
    #             #            group['SamplerLOLP'].mean(), group['SamplerLOLP'].std())

    # ###################################### Begin Pipeline Level Parallelism  Graphs ######################################
    # for system in ["RTS79", "MRTS", "RTS96"]:
    #     gThreads  = np.array([name[1] for name, group in mcsPipeline if name[0] == system], np.dtype('float64'))
    #     cThreads  = np.array([name[2] for name, group in mcsPipeline if name[0] == system], np.dtype('float64'))
    #     time      = np.array([group['TotalTime'].mean() for name, group in mcsPipeline if name[0] == system], np.dtype('float64'))

    #     sTime    = mcs.get_group(system)['TotalTime'].mean()
    #     speedup  = np.array([sTime/x for x in time], np.dtype('float64'))
    #     eff      = speedup/(gThreads + cThreads)

    #     surfacePlot(cThreads, gThreads, time,    figureTitle="", xAxisTitle='Class. Threads', yAxisTitle='Gen. Threads', zAxisTitle='Time(s)',
    #                 saveToFile = system + "_Pipeline_Time.png")
    #     surfacePlot(gThreads, cThreads, speedup, figureTitle="", xAxisTitle='Gen. Threads', yAxisTitle='Class. Threads', zAxisTitle='Time(s)',
    #                 saveToFile = system + "_Pipeline_Speedup.png")
    #     surfacePlot(gThreads, cThreads, eff,     figureTitle="", xAxisTitle='Gen. Threads', yAxisTitle='Class. Threads', zAxisTitle='Time(s)',
    #                 saveToFile = system + "_Pipeline_Eff.png")
    # #     surfacePlot(nThreads, bSize, speedup, figureTitle="", xAxisTitle='# Threads', yAxisTitle='Batch Size', zAxisTitle='Speedup')
    # #     surfacePlot(nThreads, bSize, eff,     figureTitle="", xAxisTitle='# Threads', yAxisTitle='Batch Size', zAxisTitle='Efficiency')

def func(x, pos):  # formatter function takes tick label and tick position
    s = str(x)
    s = s[0:len(s)-2]
    if len(s) > 3:
        s = s[0:len(s)-3] + "," + s[-3:]
    return s


def surfacePlot(X, Y, Z, figureTitle, xAxisTitle, yAxisTitle, zAxisTitle, saveToFile="", formatter = ticker.FuncFormatter(func)):

    fig = pyplot.figure(figsize=(15, 8.5))
    ax  = fig.gca(projection='3d')
    xi  = np.linspace(X.min(), X.max(), 100)
    yi  = np.linspace(Y.min(), Y.max(), 100)
    zi  = griddata((X, Y), Z, (xi[None,:], yi[:,None]))

    xig, yig = np.meshgrid(xi, yi)

    # color_list = cm.jet(np.linspace(0, z.max(), int(z.max()))

    surf = ax.plot_surface(xig, yig, zi, cmap=cm.CMRmap, linewidth=0, vmin=Z.min(), vmax=Z.max())
    fig.colorbar(surf, shrink=0.5)

    ax.yaxis.set_major_formatter(formatter)

    ax.set_title(figureTitle)
    ax.set_xlabel(xAxisTitle)
    ax.set_ylabel(yAxisTitle)
    ax.set_zlabel(zAxisTitle)

    if saveToFile == "":
        print("Showing...")
        pyplot.show()
    else:
        print("Saving..." + saveToFile)
        pylab.savefig(saveToFile)

if __name__ == "__main__":
    
    dbFile = 'CFO_Nieghborhoods_2016_04_28.db'

    analyzeData(dbFile)
