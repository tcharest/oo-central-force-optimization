/* This file is part of CFO in C++.

    CFO in C++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CFO in C++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CFO in C++.  If not, see <http://www.gnu.org/licenses/>.
    
    Copyright 2009 - 2012 Robert C. Green II - Robert.C.Green@gmail.com
*/

#include <iostream>
#include <iomanip>
#include <vector>
#include <math.h>
#include <cstdlib>
#include <cmath>
#include <fstream>
#include <sstream>
#include <limits>
#include <omp.h>
#include <stdlib.h> 

#include "anyoption.h"
#include "CStopWatch.h"
#include "defs.h"
#include "fitnessFunc.h"
#include "objFuncs.h"

#include "CFO.h"
#include "CFO_PF.h"
#include "CFO_Ring.h"

using namespace std;


double sigmoid(double x){           return 1/(1+exp(-x));   }

std::vector<fitnessFunction*> fitnessFuncs = { 
    new fitnessFunction(&ObjFuncs::F1,  -100,  100,      "F1" ),
    new fitnessFunction(&ObjFuncs::F2,  -10,   10,       "F2" ),
    new fitnessFunction(&ObjFuncs::F3,  -600,  600,      "F3" ),
    new fitnessFunction(&ObjFuncs::F4,  -10,   10,       "F4" ),
    new fitnessFunction(&ObjFuncs::F5,  -32,   32,       "F5" ),
 //   new fitnessFunction(&ObjFuncs::F6,  -5.12, 5.12,     "F6" ),
    new fitnessFunction(&ObjFuncs::F7,  - 4,   4,        "F7" ),
    new fitnessFunction(&ObjFuncs::F8,  -64,   64,       "F8" ),
    new fitnessFunction(&ObjFuncs::F9,  -100,  100,      "F9" ),
    new fitnessFunction(&ObjFuncs::F10, -4.5,  4.5,      "F10"),
    new fitnessFunction(&ObjFuncs::F11, -10,   10,       "F11"),
    new fitnessFunction(&ObjFuncs::F12, -15,   -3,       "F12"),
    new fitnessFunction(&ObjFuncs::F13, -3,    3,        "F13"),
    new fitnessFunction(&ObjFuncs::F14, -100,  100,      "F14"),
    new fitnessFunction(&ObjFuncs::F15, -2,      2,      "F15"),
    new fitnessFunction(&ObjFuncs::F16, -10,    10,      "F16"),
    new fitnessFunction(&ObjFuncs::F17, -10.24, 10.24,   "F17"),
    new fitnessFunction(&ObjFuncs::F18, -10.24, 10.24,   "F18"),
 // new fitnessFunction(&ObjFuncs::F19, -1.28,  1.28,    "F19"),
    new fitnessFunction(&ObjFuncs::F20, -512,   512,     "F20"),
    new fitnessFunction(&ObjFuncs::F21, -512,   512,     "F21"),
    new fitnessFunction(&ObjFuncs::F22, -10,    10,      "F22"),
    new fitnessFunction(&ObjFuncs::F23, -10.25, 10.24,   "F23")
}; 

void nieghborhoodTest(){
    double min, max;
    std::string functionName;
    double (*rPtr)(std::vector <std::vector<std::shared_ptr<Probe>>>&, int, int, int) = NULL;
    int Nd, Nt;
    double Alpha, Beta, Gamma, Frep;
    vector<double> xMin, xMax;
    CFO_PF pfCfo;
    CFO *cfo;
    
    std::vector<IPD_TYPE>         ipds;
    std::vector<CFO_TYPE>         cts;

    ipds.push_back(UNIFORM_ON_AXIS);
    ipds.push_back(UNIFORM_ON_DIAGONAL);

    cts.push_back(STANDARD);    
    cts.push_back(RING);        cts.push_back(LINEAR);
    cts.push_back(STAR);        cts.push_back(MESH);        cts.push_back(TOROIDAL);
    cts.push_back(STATIC_TREE); cts.push_back(GLOBAL_BEST); cts.push_back(GLOBAL_BESTWORST);
	cts.push_back(MULTIPLICITY);

    pfCfo.setLogToFile(false); 

    for(unsigned int x=0; x<fitnessFuncs.size(); x++){

        Nd    = 50;  Nt = 1000;
        Alpha = 1;   Beta = 2; 
        Gamma = 0.0; Frep = 0.5;

        rPtr         = fitnessFuncs[x]->fFunction(); 
        functionName = fitnessFuncs[x]->name(); 
        min          = fitnessFuncs[x]->min(); 
        max          = fitnessFuncs[x]->max();

        for(unsigned int i=0; i<ipds.size(); i++){

            for(unsigned int j=0; j<cts.size(); j++){
                xMin.clear(); xMin.resize(Nd, min);
                xMax.clear(); xMax.resize(Nd, max);

                pfCfo.setNd(Nd);        pfCfo.setNt(Nt); 
                pfCfo.setAlpha(Alpha);  pfCfo.setBeta(Beta);     pfCfo.setGamma(Gamma);
                pfCfo.setFrep(Frep);    pfCfo.setDeltaFrep(0.1);
                pfCfo.setXMin(xMin);    pfCfo.setXMax(xMax);
                
                pfCfo.setObjectiveFunction(rPtr);   pfCfo.setFunctionName(functionName);
                pfCfo.setIPDType(ipds[i]);          
                pfCfo.setUseAccelClipping(false);   pfCfo.setAMax(0.1);
                pfCfo.setPrintDelimitedResults(true);
                
                if(cts[j] == STANDARD)              { pfCfo.setCfoType(STANDARD);}
                else if(cts[j] == RING)             { pfCfo.setCfoType(RING);}
                else if(cts[j] == STAR)             { pfCfo.setCfoType(STAR);}
                else if(cts[j] == LINEAR)           { pfCfo.setCfoType(LINEAR);}
                else if(cts[j] == MESH)             { pfCfo.setCfoType(MESH);}
                else if(cts[j] == TOROIDAL)         { pfCfo.setCfoType(TOROIDAL);}
                else if(cts[j] == STATIC_TREE)      { pfCfo.setCfoType(STATIC_TREE);}
                else if(cts[j] == GLOBAL_BEST)      { pfCfo.setCfoType(GLOBAL_BEST);}
                else if(cts[j] == GLOBAL_BESTWORST) { pfCfo.setCfoType(GLOBAL_BESTWORST);}
				else if(cts[j] == MULTIPLICITY)		{ pfCfo.setCfoType(MULTIPLICITY); }

                pfCfo.run();
                pfCfo.printDelimitedResults();
                cout << endl;

                if(cts[j] == STANDARD)              { cfo = new CFO();}
                else if(cts[j] == RING)             { cfo = new CFO_Ring();}
                else if(cts[j] == STAR)             { cfo = new CFO_Star();}
                else if(cts[j] == LINEAR)           { cfo = new CFO_Linear();}
                else if(cts[j] == MESH)             { cfo = new CFO_Mesh();}
                else if(cts[j] == TOROIDAL)         { cfo = new CFO_Toroidal();}
                else if(cts[j] == STATIC_TREE)      { cfo = new CFO_Static_Tree();}
                else if(cts[j] == GLOBAL_BEST)      { cfo = new CFO_Global_Best();}
                else if(cts[j] == GLOBAL_BESTWORST) { cfo = new CFO_Global_Best_Worst();}
				else if(cts[j] == MULTIPLICITY )    { cfo = new CFO_Multiplicity(); }

                // cfo = new CFO(); 
                for(int j=0; j<10; j++){
                    cfo->setLogToFile(true);
                    cfo->setNp(pfCfo.getBestNp());  cfo->setNd(Nd);         cfo->setNt(Nt);
                    cfo->setAlpha(Alpha);           cfo->setBeta(Beta);     cfo->setGamma(pfCfo.getBestGamma());
                    cfo->setFrep(Frep);             cfo->setDeltaFrep(0.1);
                    cfo->setXMin(xMin);             cfo->setXMax(xMax);
                    
                    cfo->setObjectiveFunction(rPtr); cfo->setFunctionName(functionName);
                    cfo->setIPDType(ipds[i]);
                    cfo->setUseAccelClipping(false); cfo->setAMax(0.1);
                    
                    cfo->run();
                    cfo->printDelimitedResults();
                    cfo->recordValues();
                }
                cout << endl;
                delete cfo;
            }
            cout << endl;
        }
        
        cfo = NULL; rPtr = NULL;
    }
}

void binaryTest(){
    std::string functionName;
    //double (*rPtr)(ldArray3D&, int, int, int) = NULL;	
	double(*rPtr)(std::vector <std::vector<std::shared_ptr<Probe>>>&, int, int, int) = NULL;
    int Nd, Nt, Np;
    double Alpha, Beta, Gamma, Frep, min, max;
    vector<double> xMin, xMax;
    CFO_PF pfCfo;
    CFO *cfo;
  
    Nt = 1000;

    for(Np=110; Np<120; Np+=10){
        // Nd = 60; rPtr = &ObjFuncs::binaryF1; functionName = "F1";   // Nd/3
        // Nd = 60;  rPtr = &ObjFuncs::binaryF2; functionName = "F2"; // Nd/3
        // Nd = 60;  rPtr = &ObjFuncs::binaryF3; functionName = "F3"; // Problem           // 3.5 * Nd/5
        Nd = 60;  rPtr = &ObjFuncs::binaryF4; functionName = "F4"; // 19.1
        
        // Nd = 30;  rPtr = &ObjFuncs::binaryF6; functionName = "F5"; // 78.6
        // Nd = 24;  rPtr = &ObjFuncs::binaryF7; functionName = "F6"; // 3905.93
        // Nd = 50;  rPtr = &ObjFuncs::binaryF8;  functionName = "F7";// Problem
        // Nd = 30; rPtr = &ObjFuncs::binaryF10; functionName = "F8";
        
        Alpha = 1; Beta = 2; Gamma = 0.5;
        Frep = 0.5;
        min = -1; max = 1;
        xMin.clear(); xMin.resize(Nd, min);
        xMax.clear(); xMax.resize(Nd, max);

        cfo = new CFO_Binary();
        cfo->setLogToFile(true);
        cfo->setNp(Np);					 cfo->setNd(Nd);			cfo->setNt(Nt);
        cfo->setAlpha(Alpha);			 cfo->setBeta(Beta);		cfo->setGamma(Gamma);
        cfo->setFrep(Frep);				 cfo->setDeltaFrep(0.1);
        cfo->setXMin(xMin);				 cfo->setXMax(xMax);
        cfo->setObjectiveFunction(rPtr); cfo->setFunctionName(functionName);
        cfo->setUseAccelClipping(false); cfo->setAMax(0.1);		
        cfo->setIPDType(UNIFORM_ON_AXIS);

        for(int i=0; i<37; i++){
            cfo->run();
            cfo->printDelimitedResults();
        }
        delete cfo;
    }
    
    cfo = NULL; rPtr = NULL;
}


void basicTest(int Nt){
	std::cout << "Running basic test...\n";
    std::string functionName;
    double (*rPtr)(std::vector <std::vector<std::shared_ptr<Probe>>>&, int, int, int) = NULL;
    double min, max;
    int Nd;
    double Alpha, Beta, Gamma=0.5, Frep;
    vector<double> xMin, xMax;
    CFO_PF pfCfo;
    CFO *cfo;

    Nd = 10; rPtr = &ObjFuncs::F1;
    Alpha = 1; Beta = 2; 
    Frep = 0.5;
    min = -100; max = 100;
    xMin.clear(); xMin.resize(Nd, min);
    xMax.clear(); xMax.resize(Nd, max);

	std::cout << "Running Parameter Free CFO...\n";

    pfCfo.setLogToFile(false); 
    pfCfo.setNd(Nd);		pfCfo.setNt(Nt); 
    pfCfo.setAlpha(Alpha);	pfCfo.setBeta(Beta);     pfCfo.setGamma(Gamma);
    pfCfo.setFrep(Frep);	pfCfo.setDeltaFrep(0.1);
    pfCfo.setXMin(xMin);	pfCfo.setXMax(xMax);
            
    pfCfo.setObjectiveFunction(rPtr);	pfCfo.setFunctionName(functionName);
    pfCfo.setIPDType(UNIFORM_ON_AXIS);	
    pfCfo.setCfoType(CFO_TYPE::STANDARD);
    pfCfo.setUseAccelClipping(false);	pfCfo.setAMax(0.1);
    pfCfo.setPrintDelimitedResults(true);
            
    pfCfo.run();
    pfCfo.printDelimitedResults();
    cout << endl;	

	std::cout << "Creating CFO...\n";
    cfo = new CFO();
    //cfo = new CFO_Global_Best_Worst;
    cfo->setLogToFile(true);
    cfo->setIPDType(UNIFORM_ON_AXIS);
    cfo->setNp(pfCfo.getBestNp());	 cfo->setNd(Nd);			cfo->setNt(Nt);
    cfo->setAlpha(Alpha);			 cfo->setBeta(Beta);		cfo->setGamma(pfCfo.getBestGamma());
    cfo->setFrep(Frep);				 cfo->setDeltaFrep(0.1);
    cfo->setXMin(xMin);				 cfo->setXMax(xMax);
    cfo->setObjectiveFunction(rPtr); cfo->setFunctionName(functionName);
    cfo->setUseAccelClipping(false); cfo->setAMax(0.1);			
    
	std::cout << "Running CFO...\n";
    cfo->run(); 
    cfo->recordValues();
    cfo->printDelimitedResults();

    cout << endl;
    
    delete cfo;
    cfo = NULL; rPtr = NULL;
}


void funcTest(std::string funcName,             double (*rPtr)(std::vector <std::vector<std::shared_ptr<Probe>>>&, int, int, int),
                            double min,         double max,      double Frep = 0.5, 
                            double Alpha = 1,   double Beta = 2, double Gamma = 0.5,
                            int Np = 10,        int Nd = 10,     int Nt = 1000, 
                            int threads = 1,    bool runPF = true,   int trials = 10,
                            CFO_TYPE cfoType = STANDARD ){
   
    vector<double> xMin, xMax;
    omp_set_num_threads(threads);

    CFO *cfo;
    CFO_PF pfCfo;

    xMin.clear(); xMin.resize(Nd, min);
    xMax.clear(); xMax.resize(Nd, max);
    
    if(runPF){
        pfCfo.setLogToFile(true); 
        pfCfo.setNd(Nd);            pfCfo.setNt(Nt); 
        pfCfo.setAlpha(Alpha);      pfCfo.setBeta(Beta);            pfCfo.setGamma(Gamma);
        pfCfo.setFrep(Frep);        pfCfo.setDeltaFrep(0.1);
        pfCfo.setXMin(xMin);   
        pfCfo.setXMax(xMax);

        pfCfo.setObjectiveFunction(rPtr);       
        pfCfo.setFunctionName(funcName);
        pfCfo.setIPDType(UNIFORM_ON_AXIS);  
        pfCfo.setCfoType(cfoType);
        pfCfo.setUseAccelClipping(false);       
        pfCfo.setAMax(0.1);
        pfCfo.setPrintDelimitedResults(true);
        
        pfCfo.run();   
        
        Np    = pfCfo.getBestNp();
        Gamma = pfCfo.getBestGamma();
    }


    for(int i=0; i<trials; i++){
        if(cfoType == STANDARD)              { cfo = new CFO();}
        else if(cfoType == RING)             { cfo = new CFO_Ring();}
        else if(cfoType == STAR)             { cfo = new CFO_Star();}
        else if(cfoType == LINEAR)           { cfo = new CFO_Linear();}
        else if(cfoType == MESH)             { cfo = new CFO_Mesh();}
        else if(cfoType == TOROIDAL)         { cfo = new CFO_Toroidal();}
        else if(cfoType == STATIC_TREE)      { cfo = new CFO_Static_Tree();}
        else if(cfoType == GLOBAL_BEST)      { cfo = new CFO_Global_Best();}
        else if(cfoType == GLOBAL_BESTWORST) { cfo = new CFO_Global_Best_Worst();}
        else if(cfoType == MULTIPLICITY )    { cfo = new CFO_Multiplicity(); }

        cfo->setLogToFile(false);
        cfo->setIPDType(UNIFORM_ON_AXIS);
        cfo->setFunctionName(funcName);
        
        cfo->setNp(Np); cfo->setNd(Nd); cfo->setNt(Nt);
        cfo->setAlpha(Alpha); cfo->setBeta(Beta);  cfo->setGamma(Gamma); 
        cfo->setFrep(Frep);                 cfo->setDeltaFrep(0.1);
        cfo->setXMin(xMin);                 cfo->setXMax(xMax);
        cfo->setObjectiveFunction(rPtr);    cfo->setFunctionName(funcName);
        cfo->setUseAccelClipping(false);    cfo->setAMax(0.1);          
        cfo->setThreads(threads);
        cfo->run();
        cfo->recordValues();
        cfo->printDelimitedResults();
        cfo->printDelimitedResultsToFile();
        // cout << endl << endl;
        
        delete cfo;
        cfo = NULL; 
    }
    rPtr = NULL;
}

void multiplicityTest(){
    std::vector<CFO_TYPE> cts;

    double  Frep = 0.5, Alpha = 1.0, Beta = 2.0,
            Gamma = 0.5 ;

    int     Np = 10, Nd, Nt = 250,
            numThreads = 1, 
            numTrials = 10;

    bool runPF = true;
    
    cts.push_back(STANDARD); 
    cts.push_back(MULTIPLICITY);                       
       
    std::vector<fitnessFunction*> fitnessFuncs;
    fitnessFuncs.push_back(new fitnessFunction(&ObjFuncs::F1,  -100,  100,      "F1"));
    fitnessFuncs.push_back(new fitnessFunction(&ObjFuncs::F2,  -10,   10,       "F2"));
    fitnessFuncs.push_back(new fitnessFunction(&ObjFuncs::F3,  -600,  600,      "F3"));
    fitnessFuncs.push_back(new fitnessFunction(&ObjFuncs::F4,  -10,   10,       "F4"));

    fitnessFuncs.push_back(new fitnessFunction(&ObjFuncs::F5,  -32,   32,       "F5"));
    fitnessFuncs.push_back(new fitnessFunction(&ObjFuncs::F6,  -5.12, 5.12,     "F6"));
    fitnessFuncs.push_back(new fitnessFunction(&ObjFuncs::F7,  - 4,   4,        "F7"));
    fitnessFuncs.push_back(new fitnessFunction(&ObjFuncs::F8,  -64,   64,       "F8"));
    fitnessFuncs.push_back(new fitnessFunction(&ObjFuncs::F9,  -100,  100,      "F9"));

    fitnessFuncs.push_back(new fitnessFunction(&ObjFuncs::F10, -4.5,  4.5,      "F10"));
    fitnessFuncs.push_back(new fitnessFunction(&ObjFuncs::F11, -10,   10,       "F11"));
    fitnessFuncs.push_back(new fitnessFunction(&ObjFuncs::F12, -15,   -3,       "F12"));
    fitnessFuncs.push_back(new fitnessFunction(&ObjFuncs::F13, -3,    3,        "F13"));
    fitnessFuncs.push_back(new fitnessFunction(&ObjFuncs::F14, -100,  100,      "F14"));
    fitnessFuncs.push_back(new fitnessFunction(&ObjFuncs::F15, -2,      2,      "F15"));
    fitnessFuncs.push_back(new fitnessFunction(&ObjFuncs::F16, -10,    10,      "F16"));
    fitnessFuncs.push_back(new fitnessFunction(&ObjFuncs::F17, -10.24, 10.24,   "F17"));
    fitnessFuncs.push_back(new fitnessFunction(&ObjFuncs::F18, -10.24, 10.24,   "F18"));
    // fitnessFuncs.push_back(new fitnessFunction(&ObjFuncs::F19, -1.28,  1.28,    "F19"));
    fitnessFuncs.push_back(new fitnessFunction(&ObjFuncs::F20, -512,   512,     "F20"));
    fitnessFuncs.push_back(new fitnessFunction(&ObjFuncs::F21, -512,   512,     "F21"));
    fitnessFuncs.push_back(new fitnessFunction(&ObjFuncs::F22, -10,    10,      "F22"));
    fitnessFuncs.push_back(new fitnessFunction(&ObjFuncs::F23, -10.25, 10.24,   "F23"));

    for(Nd=10; Nd<30; Nd+=10){ 

        for(unsigned int x=0; x<fitnessFuncs.size(); x++){
            
            for(unsigned int j=0; j<cts.size(); j++){

                funcTest(fitnessFuncs[x]->name(),   fitnessFuncs[x]->fFunction(),
                            fitnessFuncs[x]->min(), fitnessFuncs[x]->max(), 
                            Frep, Alpha, Beta, Gamma,
                            Np, Nd, Nt,
                            numThreads, runPF, numTrials,
                            cts[j]); 
            }
        }
    }
}

void openMpTest(int Np, int Nd, int Nt, double Alpha, double Beta, bool threadPerProbe, int numThreads,
		bool runPF, bool optimalNp) {
    double Gamma = 0.6, Frep = 0.5;

    // Selected optimal Np values for test functions given specific values of Nd  (f19 omitted). 
    vector<int> thrArr; 
    switch (Nd){
        case 10:
            thrArr = { 2*Nd, 4*Nd, 2*Nd, 4*Nd, 120, /*120,*/ 4*Nd, 120, 120, 6*Nd, 120, 120, 
                        4*Nd, 120, 4*Nd, 120, 120, 20,  120, 100, 2*Nd, 4*Nd };
            break;
        case 20:
            thrArr = { 2*Nd, 4*Nd, 2*Nd, 4*Nd, 160, /*160,*/ 4*Nd, 160, 160, 6*Nd, 160, 160, 
                        4*Nd, 160, 4*Nd, 160, 160, 160, 160, 160, 2*Nd, 4*Nd };
            break;
        case 30:
            thrArr = { 2*Nd, 4*Nd, 2*Nd, 4*Nd, 180, /*180,*/ 4*Nd, 180, 180, 6*Nd, 180, 180, 
                        4*Nd, 180, 4*Nd, 180, 180, 180, 180, 180, 2*Nd, 4*Nd };
            break;
        default:
            cout << "Choose Nd = [10, 20, 30]"<<endl;
            return; 
    }
    
    if(threadPerProbe){
        for(unsigned int x=0; x<fitnessFuncs.size(); x++){
            funcTest( fitnessFuncs[x]->name(), fitnessFuncs[x]->fFunction(), fitnessFuncs[x]->min(), fitnessFuncs[x]->max(), 
                      Frep, Alpha, Beta, Gamma, thrArr[x], Nd, Nt, thrArr[x], runPF );
        }
    }else if(optimalNp){
        for(unsigned int x=0; x<fitnessFuncs.size(); x++){  
            funcTest( fitnessFuncs[x]->name(), fitnessFuncs[x]->fFunction(), fitnessFuncs[x]->min(), fitnessFuncs[x]->max(), 
                      Frep, Alpha, Beta, Gamma, thrArr[x], Nd, Nt, numThreads, runPF );
        }
    }
}

void threadTest(int Np, int numThreads, int Nd, int iterations){
  
    double Frep = 0.5, Gamma = 0.6;
    int Alpha = 1, Beta = 2, Nt = 1000;
    
    for(int x=0; x<iterations; x++){
        funcTest("F1",  &ObjFuncs::F1,  -10,     10,     Frep, Alpha, Beta,  Gamma,  Np, Nd, Nt, numThreads, false);
        //funcTest("F2",  &ObjFuncs::F2,  -5.13,   5.13,   Frep, Alpha, Beta,  Gamma,  Np, Nd, Nt, numThreads, false);
        //funcTest("F3",  &ObjFuncs::F3,  -512,    512,    Frep, Alpha, Beta,  Gamma,  Np, Nd, Nt, numThreads, false);
        //funcTest("F4",  &ObjFuncs::F4,  -2.048,  2.048,  Frep, Alpha, Beta,  Gamma,  Np, Nd, Nt, numThreads, false);
        //funcTest("F5",  &ObjFuncs::F5,  -32,     32,     Frep, Alpha, Beta,  Gamma,  Np, Nd, Nt, numThreads, false);
        //funcTest("F6",  &ObjFuncs::F6,  -5.12,   5.12,   Frep, Alpha, Beta,  Gamma,  Np, Nd, Nt, numThreads, false);
        //funcTest("F7",  &ObjFuncs::F7,  -4,      4,      Frep, Alpha, Beta,  Gamma,  Np, Nd, Nt, numThreads, false);
        //funcTest("F8",  &ObjFuncs::F8,  -64,     64,     Frep, Alpha, Beta,  Gamma,  Np, Nd, Nt, numThreads, false);
        //funcTest("F9",  &ObjFuncs::F9,  -100,    100,    Frep, Alpha, Beta,  Gamma,  Np, Nd, Nt, numThreads, false);
        //funcTest("F10", &ObjFuncs::F10, -4.5,     4.5,   Frep, Alpha, Beta,  Gamma,  Np, Nd, Nt, numThreads, false);
        //funcTest("F11", &ObjFuncs::F11, -10,       10,   Frep, Alpha, Beta,  Gamma,  Np, Nd, Nt, numThreads, false);
        //funcTest("F12", &ObjFuncs::F12, -15,       -3,   Frep, Alpha, Beta,  Gamma,  Np, Nd, Nt, numThreads, false);
        //funcTest("F13", &ObjFuncs::F13, -3,         3,   Frep, Alpha, Beta,  Gamma,  Np, Nd, Nt, numThreads, false);
        //funcTest("F14", &ObjFuncs::F14, -100,     100,   Frep, Alpha, Beta,  Gamma,  Np, Nd, Nt, numThreads, false);
        //funcTest("F15", &ObjFuncs::F15, -2,         2,   Frep, Alpha, Beta,  Gamma,  Np, Nd, Nt, numThreads, false);
        //funcTest("F16", &ObjFuncs::F16, -10,       10,   Frep, Alpha, Beta,  Gamma,  Np, Nd, Nt, numThreads, false);
        //funcTest("F17", &ObjFuncs::F17, -10.24, 10.24,   Frep, Alpha, Beta,  Gamma,  Np, Nd, Nt, numThreads, false);
        //funcTest("F18", &ObjFuncs::F18, -10.24, 10.24,   Frep, Alpha, Beta,  Gamma,  Np, Nd, Nt, numThreads, false);
        //funcTest("F20", &ObjFuncs::F20,  -512,     512,  Frep, Alpha, Beta,  Gamma,  Np, Nd, Nt, numThreads, false);
        //funcTest("F21", &ObjFuncs::F21,  -512,     512,  Frep, Alpha, Beta,  Gamma,  Np, Nd, Nt, numThreads, false);
        //funcTest("F22", &ObjFuncs::F22,  -10,       10,  Frep, Alpha, Beta,  Gamma,  Np, Nd, Nt, numThreads, false);
        //funcTest("F23", &ObjFuncs::F23,  -10.25, 10.24,  Frep, Alpha, Beta,  Gamma,  Np, Nd, Nt, numThreads, false);
    }
}


int main(int argc, char *argv[]) {

    /*objFuncs Testing*/
    int     Np, Nd, Nt, numThreads, iterations;
	bool    threadsPerProbe,
		runPF,
		runBasicTest,
		runOpenMpTest,
		runNeighborhoodTest,
		runBinaryTest,
		optimalNp,
		runThreadTest,
		runMultTest;
    double  Alpha, Beta;
    AnyOption *opt = new AnyOption();

    Utils::setOptions(opt);
    opt->processCommandArgs(argc, argv);

    if(opt->getFlag("help")){
        opt->printUsage();
        return 0; 
    }

    optimalNp           = opt->getFlag("optimalNp");
    runPF               = opt->getFlag("runPF");
    threadsPerProbe     = opt->getFlag("threadsPerProbe");
    runBasicTest        = opt->getFlag("basicTest");
    runOpenMpTest       = opt->getFlag("openMpTest");
    runNeighborhoodTest = opt->getFlag("neighborhoodTest");
    runBinaryTest       = opt->getFlag("binaryTest");
    runThreadTest       = opt->getFlag("threadTest");
	runMultTest			= opt->getFlag("multTest");

    opt->getFlag("nested") ? omp_set_nested(1) : omp_set_nested(0);
    
    opt->getValue("Np")      != NULL ? Np           = atoi(opt->getValue("Np"))     : Np    = 40;
    opt->getValue("Nd")      != NULL ? Nd           = atoi(opt->getValue("Nd"))     : Nd    = 20;
    opt->getValue("Nt")      != NULL ? Nt           = atoi(opt->getValue("Nt"))     : Nt    = 1000;
    opt->getValue("alpha")   != NULL ? Alpha        = atoi(opt->getValue("alpha"))  : Alpha = 1;
    opt->getValue("beta")    != NULL ? Beta         = atoi(opt->getValue("beta"))   : Beta  = 2;

    opt->getValue("iters")   != NULL ? iterations   = atoi(opt->getValue("iters"))  : iterations = 1;
    opt->getValue("threads") != NULL ? numThreads   = atoi(opt->getValue("threads")): numThreads = 1;

    omp_set_num_threads(numThreads);

    if(runBasicTest)            { basicTest(Nt); }
    else if(runOpenMpTest)      { openMpTest(Np, Nd, Nt, Alpha, Beta, threadsPerProbe, numThreads, runPF, optimalNp); }
    else if(runNeighborhoodTest){ nieghborhoodTest(); }
    else if(runBinaryTest)      { binaryTest(); }
    else if(runThreadTest)      { threadTest(Np, numThreads, Nd, iterations); }
	else if (runMultTest)		{ multiplicityTest();}
    else{ 
        cout << "Running basicTest with default Parameters\n";
        basicTest(Nt);
    }
    
    return 0;
}
