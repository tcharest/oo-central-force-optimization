#!/bin/bash
NUMS="1 1 1 1 1 1 1 1 1 1"
ND="10 20 30"
for n in $ND
do
   for threads in $NUMS
   do      
		  ./oo-central-force-optimization-multiplicity-speedy.exe --threads $threads --Nd $n --runPF
		  ./oo-central-force-optimization-standard-speedy.exe --threads $threads --Nd $n --runPF
   done
done