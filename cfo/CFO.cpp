/* This file is part of CFO in C++.

    CFO in C++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CFO in C++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CFO in C++.  If not, see <http://www.gnu.org/licenses/>.
    
    Copyright 2009 - 2012 Robert C. Green II - Robert.C.Green@gmail.com
*/

#include "CFO.h"
CFO::CFO(){
    cfoType = STANDARD;
}

CFO::CFO(   int _Np, int _Nd, int _Nt,
            double _Alpha,  double _Beta,               double _Gamma,
            double _Frep,       double _deltaFrep,  std::vector<double> min, std::vector<double> max,
            double(*_objFunc)(std::vector <std::vector<std::shared_ptr<Probe>>>& ,int, int, int),
            std::string _functionName,
            IPD_TYPE _ipdType,
            bool _useAccelClipping, double _aMax){
    ipdType = UNIFORM_ON_AXIS;
    init(_Np, _Nd, _Nt, _Alpha, _Beta, _Gamma, _Frep, _deltaFrep, min, max, _objFunc, _functionName, _ipdType, _useAccelClipping, _aMax);
}

CFO::CFO(   int _Np, int _Nd, int _Nt,
            double _Alpha,  double _Beta,               double _Gamma,
            double _Frep,       double _deltaFrep,  double min,         double max,
            double(*_objFunc)(std::vector <std::vector<std::shared_ptr<Probe>>>& ,int, int, int),
            std::string _functionName,
            IPD_TYPE _ipdType,
            bool _useAccelClipping, double _aMax) {
        
    std::vector < double > _xMin(_Np, min);
    std::vector < double > _xMax(_Np, max);
    ipdType = UNIFORM_ON_AXIS;
    init(_Np, _Nd, _Nt, _Alpha, _Beta, _Gamma, _Frep, _deltaFrep, _xMin, _xMax, _objFunc, _functionName, _ipdType, _useAccelClipping, _aMax);
}

CFO::~CFO() {
    xMin.clear(); xMax.clear();
    bestProbeNumberArray.clear();
    bestFitnessArray.clear();
    diversity.clear();
    objFunc = NULL;
}

void CFO::init( int _Np, int _Nd, int _Nt,
                double _Alpha,      double _Beta,               double _Gamma,
                double _Frep,           double _deltaFrep,  std::vector<double> _xMin, std::vector<double> _xMax,
                double(*_objFunc)(std::vector <std::vector<std::shared_ptr<Probe>>>& ,int, int, int),
                std::string _functionName,
                IPD_TYPE _ipdType,
                bool _useAccelClipping, double _aMax) {
                
    Np      = _Np;    Nd  = _Nd;    Nt = _Nt;
    Alpha   = _Alpha; Beta = _Beta; Gamma = _Gamma;
    
    Frep    = _Frep;    deltaFrep    = _deltaFrep;
    xMin    = _xMin;    xMax         = _xMax;
    objFunc = _objFunc; functionName = _functionName;
    
    originalXMin = _xMin; originalXMax = _xMax;

    useAccelClipping = _useAccelClipping; aMax = _aMax;

    ipdType = _ipdType;
    cfoType = STANDARD;

    threads = 1;
}

void CFO::UniformOnDiagonal(){
    double deltaXi;
    for(int p=0; p<Np; p++){
        for(int i=0; i<Nd; i++){
            deltaXi = (xMax[i]-xMin[i])/(Np-1);
            Probes[0][p]->Position[i] = xMin[i] + p*deltaXi;
        }
    }
}

void CFO::calculateDiagLength(){
    diagLength = 0;
    for (int i = 0; i < Nd; i++) {
        diagLength += pow(xMax[i] - xMin[i], 2);
    }
    diagLength = sqrt(diagLength);
}
void CFO::UniformOnAxis(){
    double deltaXi;

    calculateDiagLength();

    for(int i=0; i<Nd; i++) {
        for(int p=0; p<Np; p++) {
			Probes[0][p]->Position[i] = xMin[i] + Gamma * (xMax[i] - xMin[i]);
        }
    }

    for (int i = 0; i < Nd; i++) {
        deltaXi = (xMax[i] - xMin[i]) / (numProbesPerAxis-1);
        int p;
    
        for (int k = 0; k < numProbesPerAxis; k++) {
            p = k + numProbesPerAxis * i;
			Probes[0][p]->Position[i] = xMin[i] + k * deltaXi;
        }
    }
   
}
void CFO::Random(){
    MTRand mt;
    
    for (int i = 0; i < Nd; i++) {
        for (int p = 0; p < Np; p++) {
            Probes[0][p]->Position[i] = xMin[i] + mt.rand() * (xMax[i] - xMin[i]);
        }
    }
}
void CFO::Random_VDC(){
    MTRand mt;
    // double curPrime = 2;

    for (int i = 0; i < Nd; i++) {
        for (int p = 0; p < Np; p++) {
            Probes[0][p]->Position[i] = xMin[i] + Utils::corputBase(2, p*Nd+i) * (xMax[i] - xMin[i]);
        }
    }
}
void CFO::latinHypercube(){
    std::vector < std::vector < double > > tempR;
    MTRand mt;

    tempR = Utils::latinHyperCube_Random(Nd, Np, mt);
    for(int p=0; p<Np; p++){
        for(int i=0; i<Nd; i++){
            Probes[0][p]->Position[i] = xMin[i] + tempR[p][i] * (xMax[i]-xMin[i]);
        }
    }
    tempR.clear();
}
void CFO::descSampling(){
    std::vector < std::vector < double > > tempR;
    MTRand mt;

    tempR = Utils::descriptiveSampling_Random(Nd, Np, mt);
    for(int p=0; p<Np; p++){
        for(int i=0; i<Nd; i++){
            Probes[0][p]->Position[i] = xMin[i] + tempR[p][i] * (xMax[i]-xMin[i]);
        }
    }
    tempR.clear();
}
void CFO::ldsHalton(){
    std::vector < std::vector < double > > tempR;
    MTRand mt;

    tempR = Utils::haltonSampling(Nd, Np);
    for(int p=0; p<Np; p++){
        for(int i=0; i<Nd; i++){
            Probes[0][p]->Position[i] = xMin[i] + tempR[p][i] * (xMax[i]-xMin[i]);
        }
    }
    tempR.clear();
}
void CFO::ldsHammersley(){
    std::vector < std::vector < double > > tempR;
    MTRand mt;

    tempR = Utils::hammersleySampling(Nd, Np);
    for(int p=0; p<Np; p++){
        for(int i=0; i<Nd; i++){
            Probes[0][p]->Position[i] = xMin[i] + tempR[p][i] * (xMax[i]-xMin[i]);
        }
    }
    tempR.clear();
}
void CFO::ldsFaure(){
    std::vector < std::vector < double > > tempR;
    MTRand mt;

    tempR = Utils::faureSampling(Nd, Np);
    for(int p=0; p<Np; p++){
        for(int i=0; i<Nd; i++){
            Probes[0][p]->Position[i] = xMin[i] + tempR[p][i] * (xMax[i]-xMin[i]);
        }
    }
    tempR.clear();
}
void CFO::ldsHalton_Transpose(){
    std::vector < std::vector < double > > tempR;
    MTRand mt;

    tempR = Utils::haltonSampling(Np, Nd);
    for(int p=0; p<Np; p++){
        for(int i=0; i<Nd; i++){
            Probes[0][p]->Position[i] = xMin[i] + tempR[i][p] * (xMax[i]-xMin[i]);
        }
    }
    tempR.clear();
}
void CFO::ldsHammersley_Transpose(){
    std::vector < std::vector < double > > tempR;
    MTRand mt;

    tempR = Utils::hammersleySampling(Np, Nd);
    for(int p=0; p<Np; p++){
        for(int i=0; i<Nd; i++){
            Probes[0][p]->Position[i] = xMin[i] + tempR[i][p] * (xMax[i]-xMin[i]);
        }
    }
    tempR.clear();
}
void CFO::ldsFaure_Transpose(){
    std::vector < std::vector < double > > tempR;
    MTRand mt;

    tempR = Utils::faureSampling(Np, Nd);
    for(int p=0; p<Np; p++){
        for(int i=0; i<Nd; i++){
            Probes[0][p]->Position[i] = xMin[i] + tempR[i][p] * (xMax[i]-xMin[i]);
        }
    }
    tempR.clear();
}

void CFO::IPD(){
    switch(ipdType){
        case LATIN_HYPERCUBE:       latinHypercube();           break;
        case DESC_SAMP:             descSampling();             break;
        case LDS_HALTON:            ldsHalton();                break;
        case LDS_HAMMERSLEY:        ldsHammersley();            break;
        case LDS_FAURE:             ldsFaure();                 break;
        case LDS_HALTON_TRANS:      ldsHalton_Transpose();      break;
        case LDS_HAMMERSLEY_TRANS:  ldsHammersley_Transpose();  break;
        case LDS_FAURE_TRANS:       ldsFaure_Transpose();       break;
        case RANDOM:                Random();                   break;
        case RANDOM_VDC:            Random_VDC();               break;
        case UNIFORM_ON_AXIS:       UniformOnAxis();            break;
        case UNIFORM_ON_DIAGONAL:   UniformOnDiagonal();        break;
        default:                    UniformOnAxis();
    }
}

void CFO::initA() {
	// Is this really necessary?
	for (int p = 0; p < Np; p++) {
		for (int i = 0; i < Nd; i++) {
			Probes[0][p]->Acceleration[i] = 0;
		}
	}
}

void CFO::evalFitness(int j){
    timer.startTimer();
    for (int p = 0; p < Np; p++) {
		Probes[j][p]->Fitness = objFunc(Probes, Nd, p, j);
        
		numEvals++;
        
        if(Probes[j][p]->Fitness >= bestFitness){
            bestFitness     = Probes[j][p]->Fitness;
            bestTimeStep    = j;
            bestProbeNumber = p;
            
            bestFitnessArray[j]     = Probes[j][p]->Fitness;
            bestProbeNumberArray[j] = p;
        }
        if(Probes[j][p]->Fitness < worstFitness){
            worstFitness        = Probes[j][p]->Fitness;
            worstTimeStep       = j;
            worstProbeNumber    = p;
        }
    }
    timer.stopTimer();
    fitnessTime += timer.getElapsedTime();
}

void CFO::updatePositions(int j){
    timer.startTimer();
    for (int p = 0; p < Np; p++) {
        for (int i = 0; i < Nd; i++) {
			Probes[j][p]->Position[i] = Probes[j - 1][p]->Position[i] + Probes[j - 1][p]->Acceleration[i];
        }
    }
    timer.stopTimer();
    positionTime += timer.getElapsedTime();
}

void CFO::correctErrors(int j){
    timer.startTimer();
    bool isCorrected;

    for (int p = 0; p < Np; p++) {
        isCorrected = false;
        for (int i = 0; i < Nd; i++) {
            if (Probes[j][p]->Position[i] < xMin[i]) {
				Probes[j][p]->Position[i] = (std::max)(xMin[i] + Frep * (Probes[j - 1][p]->Position[i] - xMin[i]), xMin[i]);
                isCorrected = true;
            }
            if (Probes[j][p]->Position[i] > xMax[i]) {
				Probes[j][p]->Position[i] = (std::min)(xMax[i] - Frep * (xMax[i] - Probes[j - 1][p]->Position[i]), xMax[i]);
                isCorrected = true;
            }
        }
        if(isCorrected){
            numCorrections++;
        }
    }
    timer.stopTimer();
    correctionTime += timer.getElapsedTime();
}

void CFO::calculateInfluence(int p, int i, int k, int j){
    SumSQ = 0.0;
    for (int L = 0; L < Nd; L++) {
        SumSQ = SumSQ + pow(Probes[j][k]->Position[L] - Probes[j][p]->Position[L], 2);
    }
    if(SumSQ != 0){
        Denom     = sqrt(SumSQ);
        Numerator = unitStep((Probes[j][k]->Fitness - Probes[j][p]->Fitness)) * (Probes[j][k]->Fitness - Probes[j][p]->Fitness);
        Probes[j][p]->Acceleration[i] = Probes[j][p]->Acceleration[i] + (Probes[j][k]->Position[i] - Probes[j][p]->Position[i]) * pow(Numerator,Alpha)/pow(Denom,Beta);
    }
}

void CFO::updateAcceleration(int j){
    timer.startTimer();
    int p, i, k; 
    double sumSQ, denom, numerator, deltaMass;
    double alpha = Alpha;
    double beta = Beta;

    // #pragma omp parallel default(none) private(sumSQ, denom, numerator, alpha, beta, p, i, j, k)  
    // #pragma simd parallel default(none) private(sumSQ, denom, numerator, alpha, beta, p, i, j, k)  
    #pragma omp parallel default(none) private(sumSQ, denom, numerator, p, i, k, deltaMass) firstprivate(j, alpha, beta) 
    {
        #pragma omp for schedule(dynamic) 
        for ( p = 0; p < Np; p++) {
            for ( i = 0; i < Nd; i++) {

                Probes[j][p]->Acceleration[i] = 0;
                for ( k = 0; k < Np; k++) {
                    if (k != p) {
                        // calculateInfluence(p, i, k, j);
						deltaMass = Probes[j][k]->Fitness - Probes[j][p]->Fitness;
						if (deltaMass > 0)
						{
							sumSQ = 0.0;

							for (int L = 0; L < Nd; L++) {
								sumSQ = sumSQ + pow(Probes[j][k]->Position[L] - Probes[j][p]->Position[L], 2);
							}
							if (sumSQ != 0) {
								denom = sqrt(sumSQ);
								numerator = unitStep(deltaMass) * (deltaMass);
								Probes[j][p]->Acceleration[i] += (Probes[j][k]->Position[i] - Probes[j][p]->Position[i]) * pow(numerator, alpha) / pow(denom, beta);
							}
						}

                    }
                }
            }
        }
    }
    timer.stopTimer();
    accelTime += timer.getElapsedTime();
}

void CFO::clipAcceleration(int j){
    double aLength;

    timer.startTimer();

    calculateDiagLength();
    for(int p=0; p<Np; p++){
        aLength = 0;
        for(int i=0; i<Nd; i++){
            aLength += pow(Probes[j][p]->Acceleration[i],2);
        }
        aLength = sqrt(aLength);

        if(aLength > aMax * diagLength){
            for(int i=0; i<Nd; i++){
                Probes[j][p]->Acceleration[i] *= aMax;
            }
        }
    }

    timer.stopTimer();
    clippingTime += timer.getElapsedTime();
}

double CFO::unitStep(double X) {
    double retValue;
    
    if (X < 0.0) retValue = 0.0;
    else retValue = 1.0;
    
    return retValue;
}

void CFO::adjustBoundaries(int j){
    timer.startTimer();
    if(j % 10 == 0 && j >= 20){
        for(int i=0; i<Nd; i++){
            xMin[i] = xMin[i] + (Probes[bestTimeStep][bestProbeNumber]->Position[i] - xMin[i])/2.0;
            xMax[i] = xMax[i] - (xMax[i] - Probes[bestTimeStep][bestProbeNumber]->Position[i])/2.0;
        }
        correctErrors(j);
    }
    timer.stopTimer();
    shrinkTime += timer.getElapsedTime();
}

bool CFO::hasConverged(int j){
    bool converged = false;
    timer.startTimer();
    lastStep = j;
    
    if (hasFitnessSaturated(25, j)) {
            converged = true;
        }
    timer.stopTimer();
    convergeTime += timer.getElapsedTime();
    
    return converged;
}

void CFO::cleanUp(){}

void CFO::reset(){

    bestTimeStep     = 0;  worstTimeStep    = 0;
    bestProbeNumber  = 0;  worstProbeNumber = 0;
    numProbesPerAxis = Np/Nd;
    if(numProbesPerAxis == 0){ numProbesPerAxis = 1;}

    numEvals         = 0;
    numCorrections   = 0;
    
#ifdef _WIN32
    bestFitness  = -std::numeric_limits<double>::infinity();
    worstFitness = std::numeric_limits<double>::infinity();
#else
    bestFitness  = -INFINITY;
    worstFitness = -INFINITY;
#endif
    positionTime = 0, correctionTime = 0, 
    fitnessTime  = 0, accelTime      = 0, 
    shrinkTime   = 0, convergeTime   = 0,
    totalTime    = 0;
    
    bestProbeNumberArray.clear();
    bestFitnessArray.clear();
    diversity.clear();
    
	Probes.resize(Nt);
	for (auto& i : Probes) {
		i.clear();
		i.reserve(Np);
		for (int j = 0; Np != j; ++j) {
			i.push_back(std::make_shared<Probe>(Nd));
		}
	}

    bestProbeNumberArray.resize(Nt,0);
    bestFitnessArray.resize(Nt,0);
    
    xMin = originalXMin;
    xMax = originalXMax;
}

void CFO::calculateDiversity(int j){
    double sum, innerSum, diagSum, longestDiag;
    std::vector<double> avg(Np, 0.0);

    // Calculate Average Probe Value
    for(int i=0; i<Nd; i++){
        for(int p=0; p<Np; p++){
            avg[i] += Probes[j][p]->Position[i];
        }
        avg[i] /= Np;
    }

    sum      = 0;
    innerSum = 0;
    for(int p=0; p<Np; p++){
        innerSum = 0;
        for(int i=0; i<Nd; i++){
            innerSum += pow(Probes[j][p]->Position[i]-avg[i], 2);
        }
        sum += sqrt(innerSum);
    }

    // find the longest diagonal
    longestDiag = 0;
    for(int p=0; p<Np; p++){
        diagSum = 0;
        for(int l=p+1; l<Np; l++){
            for(int i=0; i<Nd; i++){
                diagSum += pow(Probes[j][p]->Position[i]-avg[i], 2);
            }
        }
        diagSum = sqrt(diagSum);
        if(diagSum > longestDiag){ longestDiag = diagSum;}
    }

    if(longestDiag == 0){ sum = 0.0;}
    else                { sum *= 1.0/(Np*longestDiag);}
    diversity.push_back(sum);

    avg.clear();
}

double CFO::run(){
    timer1.startTimer();
            
    reset();                // Make sure everything is at original settings
    IPD();                  // Initial Probe Distribution
	initA();                // Zero acceleration vectors
    evalFitness(0); 

    lastStep = Nt;
    for (int j = 1; j < Nt; j++){
        updatePositions(j);
        correctErrors(j);
        evalFitness(j);
        updateAcceleration(j);

        if(useAccelClipping){
            clipAcceleration(j);
        }
    
        // Adjust Frep
        Frep += deltaFrep;
        if(Frep > 1.0) Frep = 0.05;

        adjustBoundaries(j);
		/*
        if(hasConverged(j)){
            lastStep = j;
            break;
        }*/
        calculateDiversity(j);
    }//End Time Steps
    timer1.stopTimer();
    totalTime += timer1.getElapsedTime();
    
    cleanUp();

    return bestFitness;
}

bool CFO::hasFitnessSaturated(int nSteps, int j) {

    double fitnessSatTol    = 0.000001;
    #ifdef _WIN32
        double lBestFitness = -std::numeric_limits<double>::infinity();
    #else
        double lBestFitness  = -INFINITY;
    #endif
    #ifdef _WIN32
        double bestFitnessStepJ = -std::numeric_limits<double>::infinity();
    #else
        double bestFitnessStepJ  = -INFINITY;
    #endif
    double sumOfBestFitness = 0;
    bool retValue = false;

    if (j < nSteps + 10){
        retValue = false;
    }else{
        for (int k = j - nSteps + 1; k <= j; k++) {

            #ifdef _WIN32
                lBestFitness = -std::numeric_limits<double>::infinity();
            #else
                lBestFitness = -INFINITY;
            #endif
            for (int p = 0; p < Np; p++) {
                if (Probes[k][p]->Fitness >= lBestFitness) {
                    lBestFitness = Probes[k][p]->Fitness;
                }
            }
            if(k == j){                     
                bestFitnessStepJ = lBestFitness;
            }
            sumOfBestFitness += lBestFitness;
        }
        if (fabs(sumOfBestFitness / nSteps - bestFitnessStepJ) <= fitnessSatTol) {
            retValue = true;
        }
    }
    return retValue;
}

int CFO::getBestTimeStep()          { return bestTimeStep;      }
int CFO::getBestProbeNumber()       { return bestProbeNumber;   }
int CFO::getLastStep()              { return lastStep;          }
int CFO::getNumEvals()              { return numEvals;          }
int CFO::getNumCorrections()        { return numCorrections;    }
double CFO::getBestFitness()        { return bestFitness;       }
double CFO::getPositiontime()       { return positionTime;      }
double CFO::getCorrectionTime()     { return correctionTime;    }
double CFO::getFitnessTime()        { return fitnessTime;       }
double CFO::getAccelTime()          { return accelTime;         }
double CFO::getShrinkTime()         { return shrinkTime;        }
double CFO::getConvergeTime()       { return convergeTime;      }
std::vector<int>    CFO::getBestProbeNumberArray()  { return bestProbeNumberArray;    }
std::vector<double> CFO::getBestFitnessArray()      { return bestFitnessArray;          }

void CFO::setNp(int d)              { Np = d;          }
void CFO::setNd(int d)              { Nd = d;          }
void CFO::setNt(int d)              { Nt = d;          }
void CFO::setAlpha(double a)        { Alpha = a;       }
void CFO::setBeta(double b)         { Beta = b;        }
void CFO::setGamma(double g)        { Gamma = g;       }
void CFO::setFrep(double fr)        { Frep = fr;       }
void CFO::setDeltaFrep(double dfr)  { deltaFrep = dfr; }
void CFO::setThreads(int thr)       { threads = thr;   }

void CFO::setXMin(double min){ 
    xMin.clear(); originalXMin.clear();
    xMin.resize(Nd, min);
    originalXMin.resize(Nd, min);
}
void CFO::setXMax(double max){ 
    xMax.clear(); originalXMax.clear();
    xMax.resize(Nd, max);
    originalXMax.resize(Nd, max);    
}
void CFO::setXMin(std::vector<double> min){ 
    xMin.clear(); 
    originalXMin.clear();
    for(unsigned int i=0; i<min.size(); i++){
        xMin.push_back(min[i]);
        originalXMin.push_back(min[i]);
    }
}
void CFO::setXMax(std::vector<double> max){ 
    xMax.clear(); originalXMax.clear();
    for(unsigned int i=0; i<max.size(); i++){
        xMax.push_back(max[i]);
        originalXMax.push_back(max[i]);
    }
}
void CFO::setLogToFile(bool ltf)                        { logTofile = ltf;          }
void CFO::setFunctionName(std::string fname)            { functionName = fname;     }
void CFO::setAMax(double _aMax)                         { aMax = _aMax;             }
void CFO::setIPDType(IPD_TYPE _ipdType)                 { ipdType = _ipdType;       }
void CFO::setUseAccelClipping(bool _useAccelClipping)   { useAccelClipping = _useAccelClipping;}
void CFO::setObjectiveFunction(double(*of)(std::vector <std::vector<std::shared_ptr<Probe>>>& ,int, int, int)){ objFunc = of; }

void CFO::printResults(){
    std::cout << std::setw(23) << std::left << "Function Name:"    << functionName << std::endl;
    std::cout << std::setw(23) << std::left << "Best Fitness:"     << std::setprecision(10) << bestFitness << std::endl;
    std::cout << std::setw(23) << std::left << "Number of Probes:" << Np << std::endl;
    std::cout << std::setw(23) << std::left << "Probes Per Axis:"  << numProbesPerAxis << std::endl;
    std::cout << std::setw(23) << std::left << "Best Probe #:"     << std::setprecision(0) << bestProbeNumber << std::endl;
    std::cout << std::setw(23) << std::left << "Best Gamma:"       << std::setprecision(2) << Gamma << std::endl;
    std::cout << std::setw(23) << std::left << "Best Time Step:"   << bestTimeStep << std::endl;
    std::cout << std::setw(23) << std::left << "Last Step:"        << std::setprecision(0) << lastStep << std::endl;
    std::cout << std::setw(23) << std::left << "Evaluations:"      << std::setprecision(0) << numEvals << std::endl;
    std::cout << std::setw(23) << std::left << "Corrections:"       << std::setprecision(0) << numCorrections << std::endl;

    std::cout << "-------------------------- Times --------------------------"           << std::endl;
    std::cout << std::setw(23) << std::left << "Position:"      << std::setprecision(4) << positionTime      << std::endl;
    std::cout << std::setw(23) << std::left << "Correction:"    << std::setprecision(4) << correctionTime << std::endl;
    std::cout << std::setw(23) << std::left << "Fitness:"       << std::setprecision(4) << fitnessTime   << std::endl;
    std::cout << std::setw(23) << std::left << "Acceleration:"  << std::setprecision(4) << accelTime     << std::endl;
    std::cout << std::setw(23) << std::left << "Clipping:"      << std::setprecision(4) << clippingTime      << std::endl;
    std::cout << std::setw(23) << std::left << "Shrink:"        << std::setprecision(4) << shrinkTime    << std::endl;
    std::cout << std::setw(23) << std::left << "Converge:"      << std::setprecision(4) << convergeTime      << std::endl;
    // std::cout << std::endl;
}

void CFO::recordValues()
{
    Utils::setupDb();

    Utils::recordValues(functionName, bestFitness, bestProbeNumber, bestTimeStep, Gamma, 
        Np * Nd, Np, Nd, lastStep, numEvals, numCorrections, positionTime, correctionTime,
        fitnessTime, accelTime, shrinkTime, convergeTime, totalTime, threads, cfoType, ipdType);
}

void CFO::printDelimitedResults(std::string del){
    std::cout   << functionName                 << del
                << Utils::cfoTypeString(cfoType)  << del
                << Utils::ipdString(ipdType)    << del
                << bestFitness                  << del
                << bestProbeNumber              << del
                << bestTimeStep                 << del
                << Gamma                        << del
                << numProbesPerAxis             << del
                << Np                           << del
                << threads                      << del
                << Nd                           << del
                << lastStep                     << del
                << numEvals                     << del
                << numCorrections               << del
                << positionTime                 << del
                << correctionTime               << del
                << fitnessTime                  << del
                << accelTime                    << del
                << shrinkTime                   << del
                << convergeTime                 << del
                << totalTime                    << std::endl;

}

void CFO::printDelimitedResultsToFile(std::string del){
  std::ofstream myFile;
        std::string fileName;
        
        fileName = "../Results/CFO_" + functionName + "_" + Utils::cfoTypeString(cfoType) + "_" + Utils::ipdString(ipdType) + ".csv";
        fileName = "../Results/CFO_" + functionName + ".csv";
        myFile.open(fileName.c_str(), std::ios::app);
        
        if(myFile.is_open()){

            myFile  << functionName                 << del
                    << Utils::cfoTypeString(cfoType)<< del
                    << Utils::ipdString(ipdType)    << del
                    << bestFitness                  << del
                    << bestProbeNumber              << del
                    << bestTimeStep                 << del
                    << Gamma                        << del
                    << numProbesPerAxis             << del
                    << Np                           << del
                    << threads                      << del
                    << Nd                           << del
                    << lastStep                     << del
                    << numEvals                     << del
                    << numCorrections               << del
                    << positionTime                 << del
                    << correctionTime               << del
                    << fitnessTime                  << del
                    << accelTime                    << del
                    << shrinkTime                   << del
                    << convergeTime                 << del
                    << totalTime                    << std::endl;
        }
        myFile.close();

        /*
        fileName = "CFO_Conv_" + functionName;
        fileName += "_" + Utils::cfoTypeString(cfoType) + "_";
        fileName += "_" + Utils::ipdString(ipdType) + "_";
        fileName = Utils::changeBase(fileName, Np);
        fileName += "_"; 
        fileName = Utils::changeBase(fileName, Nd);
        fileName = fileName + ".csv";
        myFile.open(fileName.c_str(), std::ios::trunc);
        if(myFile.is_open()){
            for(int i=0; i<lastStep; i++){
                myFile << i << " " << bestFitnessArray[i] << std::endl;
            }
        }
        myFile.close();

        fileName = "CFO_Probe_Conv_Pos_" + functionName + "_";
        fileName += Utils::cfoTypeString(cfoType) + "_";
        fileName += Utils::ipdString(ipdType) + "_";
        fileName = Utils::changeBase(fileName, Np);
        fileName += "_"; 
        fileName = Utils::changeBase(fileName, Nd);
        fileName = fileName + ".csv";
        myFile.open(fileName.c_str(), std::ios::trunc);
        if(myFile.is_open()){
            for(int j=0; j<lastStep; j++){
                for(int p=0; p<Np; p++){
                    for(int i=0; i<Nd; i++){
                        myFile << Probes[j][p]->Position[i] << " ";
                    }
                }
                myFile << std::endl;
            }
        }
        myFile.close();

        fileName = "CFO_Probe_Conv_Fitness_" + functionName + "_";
        fileName += Utils::cfoTypeString(cfoType) + "_";
        fileName += Utils::ipdString(ipdType) + "_";
        fileName = Utils::changeBase(fileName, Np);
        fileName += "_"; 
        fileName = Utils::changeBase(fileName, Nd);
        fileName = fileName + ".csv";
        myFile.open(fileName.c_str(), std::ios::trunc);
        if(myFile.is_open()){
            for(int j=0; j<lastStep; j++){
                myFile << bestProbeNumberArray[j] << " ";
                for(int p=0; p<Np; p++){
                    myFile << Probes[j][p]->Fitness << " ";
                }
                myFile << std::endl;
            }
        }
        myFile.close();

        fileName = "CFO_Diversity_" + functionName;
        fileName += "_" + Utils::cfoTypeString(cfoType) + "_";
        fileName += "_" + Utils::ipdString(ipdType) + "_";
        fileName = Utils::changeBase(fileName, Np);
        fileName = fileName + "_"; 
        fileName = Utils::changeBase(fileName, Nd);
        fileName = fileName + ".csv";
        myFile.open(fileName.c_str(), std::ios::trunc);
        if(myFile.is_open()){
            for(unsigned int i=0; i<diversity.size(); i++){
                myFile << i << " " << diversity[i] << std::endl;
            }
        }
        myFile.close();
        */
}
