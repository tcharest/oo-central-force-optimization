/* This file is part of CFO in C++.

    CFO in C++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CFO in C++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CFO in C++.  If not, see <http://www.gnu.org/licenses/>.
    
    Copyright 2009 - 2012 Robert C. Green II - Robert.C.Green@gmail.com
*/

#include "CFO_Ring.h"

CFO_Ring::CFO_Ring(){
    numNeighborsLeft  = 1;
    numNeighborsRight = 1;
    cfoType = RING;
}

CFO_Ring::CFO_Ring(	int _Np, int _Nd, int _Nt,
            double _Alpha, double _Beta, double _Gamma,
            double _Frep, double _deltaFrep, std::vector<double> min, std::vector<double> max,
            double(*_objFunc)(std::vector <std::vector<std::shared_ptr<Probe>>>& ,int, int, int),
            std::string _functionName,
            IPD_TYPE _ipdType,
            bool _useAccelClipping, double _aMax){

    init(_Np, _Nd, _Nt, _Alpha, _Beta, _Gamma, _Frep, _deltaFrep, min, max, _objFunc, _functionName, _ipdType, _useAccelClipping, _aMax);
    cfoType = RING;
    numNeighborsLeft  = 1;
    numNeighborsRight = 1;
}

CFO_Ring::CFO_Ring(	int _Np, int _Nd, int _Nt,
            double _Alpha, double _Beta, double _Gamma,
            double _Frep, double _deltaFrep, double min, double max,
            double(*_objFunc)(std::vector <std::vector<std::shared_ptr<Probe>>>& ,int, int, int),
            std::string _functionName,
            IPD_TYPE _ipdType,
            bool _useAccelClipping , double _aMax) {

    std::vector < double > _xMin(_Np, min);
    std::vector < double > _xMax(_Np, max);
    init(_Np, _Nd, _Nt, _Alpha, _Beta, _Gamma, _Frep, _deltaFrep, _xMin, _xMax, _objFunc, _functionName, _ipdType, _useAccelClipping, _aMax);
    
    cfoType			  = RING;
    numNeighborsLeft  = 1;
    numNeighborsRight = 1;
}
CFO_Ring::~CFO_Ring(){}

void CFO_Ring::calculateInfluence(int p, int i, int k, int j){
    SumSQ = 0.0;
    for (int L = 0; L < Nd; L++) {
        SumSQ = SumSQ + pow(Probes[j][k]->Position[L] - Probes[j][p]->Position[L], 2);
    }
    if(SumSQ != 0){
        Denom = sqrt(SumSQ);
        Numerator = unitStep((Probes[j][k]->Fitness - Probes[j][p]->Fitness)) * (Probes[j][k]->Fitness- Probes[j][p]->Fitness);
        Probes[j][p]->Acceleration[i] = Probes[j][p]->Acceleration[i] + (Probes[j][k]->Position[i] - Probes[j][p]->Position[i]) * pow(Numerator,Alpha)/pow(Denom,Beta);
    }
}
void CFO_Ring::updateAcceleration(int j){
    timer.startTimer();
    int neighbor;

    for (int p = 0; p < Np; p++) {
        // Zero Out Acceleration
        for(int i = 0; i < Nd; i++) {
            Probes[j][p]->Acceleration[i] = 0;

            //Do the Right
            for(int n=0; n<numNeighborsRight; n++){
                neighbor = (p+(n+1)) % Np;
                calculateInfluence(p, i, neighbor, j);
            }

            //Do the left
            for(int n=0; n<numNeighborsLeft; n++){
                neighbor = (p-(n+1));
                if(neighbor < 0){ neighbor = Np + neighbor;	}
                calculateInfluence(p, i, neighbor, j);
            }
        }
    }
    timer.stopTimer();
    accelTime += timer.getElapsedTime();
}

void CFO_Ring::setNumNeighboors(int numNieghbors){
    if(numNieghbors > 0)  { 
        numNeighborsLeft  = numNieghbors;
        numNeighborsRight  = numNieghbors;
    }else{ 
        numNeighborsLeft  = 1;
        numNeighborsRight = 1;
    }
}
void CFO_Ring::setNumNeighboors(int leftNeighbors, int rightNeighbors){
    if(leftNeighbors > 0) { numNeighborsLeft  = leftNeighbors;}
    else				  { numNeighborsLeft  = 1;}

    if(rightNeighbors > 0){ numNeighborsRight = rightNeighbors;}
    else				  { numNeighborsRight = 1;}
}
