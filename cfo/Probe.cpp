#include "Probe.h"

Probe::Probe(int Nd)
{
	Acceleration = std::vector<double>(Nd, 0);
	Position = std::vector<double>(Nd, 0);
	Fitness = -std::numeric_limits<double>::infinity();
}
Probe::~Probe() {
	Acceleration.clear();
	Position.clear();
}
