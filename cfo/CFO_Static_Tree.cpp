/* This file is part of CFO in C++.

    CFO in C++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CFO in C++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CFO in C++.  If not, see <http://www.gnu.org/licenses/>.
    
    Copyright 2009 - 2012 Robert C. Green II - Robert.C.Green@gmail.com
*/

#include "CFO_Static_Tree.h"

CFO_Static_Tree::CFO_Static_Tree(){
    cfoType = STATIC_TREE;
}

CFO_Static_Tree::CFO_Static_Tree(	int _Np, int _Nd, int _Nt,
            double _Alpha, double _Beta, double _Gamma,
            double _Frep, double _deltaFrep, std::vector<double> min, std::vector<double> max,
            double(*_objFunc)(std::vector <std::vector<std::shared_ptr<Probe>>>& ,int, int, int),
            std::string _functionName,
            IPD_TYPE _ipdType,
            bool _useAccelClipping, double _aMax){

    init(_Np, _Nd, _Nt, _Alpha, _Beta, _Gamma, _Frep, _deltaFrep, min, max, _objFunc, _functionName, _ipdType, _useAccelClipping, _aMax);
    cfoType = STATIC_TREE;
}

CFO_Static_Tree::CFO_Static_Tree(	int _Np, int _Nd, int _Nt,
            double _Alpha, double _Beta, double _Gamma,
            double _Frep, double _deltaFrep, double min, double max,
            double(*_objFunc)(std::vector <std::vector<std::shared_ptr<Probe>>>& ,int, int, int),
            std::string _functionName,
            IPD_TYPE _ipdType,
            bool _useAccelClipping , double _aMax) {

    std::vector < double > _xMin(_Np, min);
    std::vector < double > _xMax(_Np, max);
    init(_Np, _Nd, _Nt, _Alpha, _Beta, _Gamma, _Frep, _deltaFrep, _xMin, _xMax, _objFunc, _functionName, _ipdType, _useAccelClipping, _aMax);
    
    cfoType = STATIC_TREE;
}
CFO_Static_Tree::~CFO_Static_Tree(){}

void CFO_Static_Tree::calculateInfluence(int p, int i, int k, int j){
    SumSQ = 0.0;
    for (int L = 0; L < Nd; L++) {
        SumSQ = SumSQ + pow(Probes[j][k]->Position[L] - Probes[j][p]->Position[L], 2);
    }
    if(SumSQ != 0){
        Denom = sqrt(SumSQ);
        Numerator = unitStep((Probes[j][k]->Fitness - Probes[j][p]->Fitness)) * (Probes[j][k]->Fitness - Probes[j][p]->Fitness);
        Probes[j][p]->Acceleration[i] = Probes[j][p]->Acceleration[i] + (Probes[j][k]->Position[i] - Probes[j][p]->Position[i]) * pow(Numerator,Alpha)/pow(Denom,Beta);
    }
}
void CFO_Static_Tree::updateAcceleration(int j){
    timer.startTimer();
    int left, right;
    
    for (int p = 0; p < Np; p++) {
        left  = (p+1)*2;
        right = (p+1)*2+1;
        
        for(int i = 0; i < Nd; i++) {
            Probes[j][p]->Acceleration[i] = 0; // Zero Out Acceleration
            calculateInfluence(p, i, bestProbeNumber, j);
            if(left < Np) { calculateInfluence(p, i, left, j);}
            if(right< Np) { calculateInfluence(p, i, right, j);}
        }
    }
    timer.stopTimer();
    accelTime += timer.getElapsedTime();
}