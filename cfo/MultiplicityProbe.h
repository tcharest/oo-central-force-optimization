#ifndef MULTIPLICITYPROBE_H_
#define MULTIPLICITYPROBE_H_
#include "Probe.h"
class MultiplicityProbe : public Probe {
public:
	MultiplicityProbe(int Nd);
	int MultiplicityFactor;
};
#endif