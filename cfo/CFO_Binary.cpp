/* This file is part of CFO_Binary in C++.

    CFO_Binary in C++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CFO_Binary in C++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CFO_Binary in C++.  If not, see <http://www.gnu.org/licenses/>.
    
    Copyright 2009 - 2012 Robert C. Green II - Robert.C.Green@gmail.com
*/

#include "CFO_Binary.h"
CFO_Binary::CFO_Binary(){
    cfoType = STANDARD;
}

CFO_Binary::CFO_Binary(	int _Np, int _Nd, int _Nt,
            double _Alpha, double _Beta, double _Gamma,
            double _Frep, double _deltaFrep, 
            double(*_objFunc)(std::vector <std::vector<std::shared_ptr<Probe>>>& ,int, int, int),
            std::string _functionName,
            IPD_TYPE _ipdType,
            bool _useAccelClipping, double _aMax){
    
    init(_Np, _Nd, _Nt, _Alpha, _Beta, _Gamma, _Frep, _deltaFrep, _objFunc, _functionName, _ipdType, _useAccelClipping, _aMax);
}

void CFO_Binary::init(	int _Np, int _Nd, int _Nt,
                double _Alpha, double _Beta, double _Gamma,
                double _Frep, double _deltaFrep,
                double(*_objFunc)(std::vector <std::vector<std::shared_ptr<Probe>>>& ,int, int, int),
                std::string _functionName,
                IPD_TYPE _ipdType,
                bool _useAccelClipping, double _aMax) {
                
    Np      = _Np;    Nd  = _Nd; 	Nt = _Nt;
    Alpha   = _Alpha; Beta = _Beta; Gamma = _Gamma;
    
    Frep    = _Frep;    deltaFrep    = _deltaFrep;
    objFunc = _objFunc; functionName = _functionName;
    
    xMin.resize(Nd, 0);	  xMax.resize(Nd, 1);
    originalXMin = xMin; originalXMax = xMax;

    useAccelClipping = _useAccelClipping; aMax = _aMax;

    ipdType = _ipdType;
    cfoType = STANDARD;
}

void CFO_Binary::calculateDiversity(int j){
    double sum;

    sum = 0;
    for(int p=0; p<Np-1; p++){
        for(int l=p+1; l<Np; l++){
            for(int i=0; i<Nd; i++){
                sum += abs(Probes[j][p]->Position[i]-Probes[j][l]->Position[i]);
            }
        }
    }

    sum = sum * 2.0;
    sum = sum /(Np*(Np-1)*Nd);
    diversity.push_back(sum);
}
void CFO_Binary::evalFitness(int j){
    timer.startTimer();

    for (int p = 0; p < Np; p++) {
        Probes[j][p]->Fitness = objFunc(Probes, Nd, p, j);
        numEvals++;
        
        if(Probes[j][p]->Fitness >= bestFitness){
            bestFitness 	= Probes[j][p]->Fitness;
            bestTimeStep 	= j;
            bestProbeNumber = p;
            
            bestFitnessArray[j]		= Probes[j][p]->Fitness;
            bestProbeNumberArray[j] = p;
        }
    }
    timer.stopTimer();
    fitnessTime += timer.getElapsedTime();
}
double CFO_Binary::sigmoid(double x){
    return 1/(1+exp(-x));
}

void CFO_Binary::reset(){

    bestTimeStep     = 0; 
    bestProbeNumber	 = 0;
    numProbesPerAxis = Np/Nd;
    numEvals		 = 0;
    numCorrections   = 0;
    
#ifdef _WIN32
    bestFitness = -std::numeric_limits<double>::infinity();
#else
    bestFitness  = -INFINITY;
#endif
    positionTime = 0, correctionTime = 0, 
    fitnessTime  = 0, accelTime      = 0, 
    shrinkTime   = 0, convergeTime   = 0,
    totalTime    = 0;
    
    bestProbeNumberArray.clear();
    bestFitnessArray.clear();
    diversity.clear(); 

    bestProbeNumberArray.resize(Nt,0);
    bestFitnessArray.resize(Nt,0);
    
    xMin = originalXMin;
    xMax = originalXMax;
}
void CFO_Binary::IPD(){
    timer.startTimer();
    Primes primes;
    double curPrime, k;

    for (int p = 0; p < Np; p++) {
        curPrime = primes.nextPrime();
        for (int i = 0; i < Nd; i++) {    
            k = 1-Utils::corputBase(curPrime, i);

            if(sigmoid(Probes[0][p]->Acceleration[i]) < k)  { Probes[0][p]->Position[i] = 0;}
            else					     { Probes[0][p]->Position[i] = 1;}
        }
    }
    timer.stopTimer();
    positionTime += timer.getElapsedTime();
    /*
    std::vector<double> dist(Np, 0);
    std::vector<double> meanDist(2, 0.0);
    std::vector<double> varDist(2, 0.0);

    for(int i=0; i<Nd; i++){
        Probes[0][0]->Position[i] = 0;
    }

    for(int p=1; p<Np; p++){
        for(int i=0; i<Nd; i++){
            // Try 0
            Probes[0][p]->Position[i] = 0;

            for(int k=0; k<p; k++){ // Every Other Particle
                dist[k] = 0;
                for(int l=0; l<Nd; l++){ // Every Other Dimension
                    if(Probes[0][p]->Position[l] != Probes[0][k]->Position[l]){ dist[k]++;}
                }
            }

            meanDist[0] = 0;
            for(int x=0; x<p; x++){meanDist[0] += dist[x]; }
            meanDist[0] = meanDist[0]/p;

            varDist[0] = 0;
            for(int x=0; x<p; x++){
                varDist[0] += (dist[x]-meanDist[0])*(dist[x]-meanDist[0]); 
            }


            // Try 1
            Probes[0][p]->Position[i] = 1;
            for(int k=0; k<p; k++){ // Every Other Particle
                dist[k] = 0;
                for(int l=0; l<Nd; l++){ // Every Other Dimension
                    if(Probes[0][p]->Position[l] != Probes[0][k]->Position[l]){ dist[k]++;}
                }
            }
            meanDist[1] = 0;
            for(int x=0; x<p; x++){meanDist[1] += dist[x]; }
            meanDist[1] = meanDist[1]/p;

            varDist[1] = 0;
            for(int x=0; x<p; x++){
                varDist[1] += (dist[x]-meanDist[1])*(dist[x]-meanDist[1]); 
            }

            if(meanDist[0] < meanDist[1]){ 
                Probes[0][p]->Position[i] = 1;
                continue;
            }
            if(meanDist[0] > meanDist[1]){ 
                Probes[0][p]->Position[i] = 0;
                continue;
            }  
            if(varDist[0] > varDist[1] ){ Probes[0][p]->Position[i] = 1;}
            else                        { Probes[0][p]->Position[i] = 0;}
        }
    }

    for(int i=0; i<Nd; i++){
        if(Utils::corputBase(2, i) < 0.5){
            
            if(Utils::corputBase(2, i+1) < 0.3) { Probes[0][0]->Position[i]    = 0;}
            else                                { Probes[0][0]->Position[i]    = 1;}
            if(Utils::corputBase(2, i+2) < 0.6) { Probes[0][1]->Position[i]    = 0;}
            else                                { Probes[0][0]->Position[i]    = 1;}
            if(Utils::corputBase(2, i+3) < 0.9) { Probes[0][Np-1]->Position[i] = 1;}
            else                                { Probes[0][0]->Position[i]    = 0;}

        }else{

            if(Utils::corputBase(2, i+1) < 0.3) { Probes[0][0]->Position[i]    = 1;}
            else                                { Probes[0][0]->Position[i]    = 0;}
            if(Utils::corputBase(2, i+2) < 0.6) { Probes[0][1]->Position[i]    = 1;}
            else                                { Probes[0][0]->Position[i]    = 0;}
            if(Utils::corputBase(2, i+3) < 0.9) { Probes[0][Np-1]->Position[i] = 0;}
            else                                { Probes[0][0]->Position[i]    = 1;}
        }
    }	
    */
}

void CFO_Binary::updatePositions(int j){
    timer.startTimer();
    Primes primes(j);
    double curPrime, k;

    for (int p = 0; p < Np; p++) {
        curPrime = primes.nextPrime();
        for (int i = 0; i < Nd; i++) {    
            //k = 1-Utils::corputBase(curPrime, p*j+i);
            k = 1-Utils::corputBase(curPrime, Np*j+i);
            if(sigmoid(Probes[j-1][p]->Acceleration[i]) < k) { Probes[j][p]->Position[i] = 0;}
            else					      { Probes[j][p]->Position[i] = 1;}
        }
    }
    timer.stopTimer();
    positionTime += timer.getElapsedTime();
}

void CFO_Binary::initA(){

    std::vector < std::vector < double > > tempR;
    MTRand mt;

    tempR = Utils::haltonSampling(Nd, Np);
    for(int p=0; p<Np; p++){
        for(int i=0; i<Nd; i++){
            Probes[0][p]->Acceleration[i] = tempR[p][i];
            //Probes[0][p]->Acceleration[i] = mt.rand();
        }
    }
    tempR.clear();
}

void CFO_Binary::updateAcceleration(int j){
    timer.startTimer();
    timer.startTimer();
    std::vector<double> maxA, minA;

     #ifdef _MSC_VER
        maxA.resize(Nd, -std::numeric_limits<double>::infinity());
        minA.resize(Nd, std::numeric_limits<double>::infinity());
    #else
        maxA.resize(Nd, -INFINITY);
        minA.resize(Nd, INFINITY);
    #endif
    
    // Update
    for (int p = 0; p < Np; p++) {
        for (int i = 0; i < Nd; i++) {
            Probes[j][p]->Acceleration[i] = 0;

            for (int k = 0; k < Np; k++) {
                if (k != p) {
                    SumSQ = 0.0;
                    for (int L = 0; L < Nd; L++) {	
                        SumSQ = SumSQ + pow(Probes[j][k]->Position[L] - Probes[j][p]->Position[L], 2);
                    }

                    if(SumSQ != 0){
                        Numerator = unitStep((Probes[j][k]->Fitness - Probes[j][p]->Fitness)) * (Probes[j][k]->Fitness- Probes[j][p]->Fitness);
                        Denom      = sqrt(SumSQ);    
                        Probes[j][p]->Acceleration[i] = Probes[j][p]->Acceleration[i] + (Probes[j][k]->Position[i] - Probes[j][p]->Position[i]) * pow(Numerator,Alpha)/pow(Denom,Beta);
                    }
                }
            }
            if(Probes[j][p]->Acceleration[i] > maxA[i]){ maxA[i] = Probes[j][p]->Acceleration[i];}
            if(Probes[j][p]->Acceleration[i] < minA[i]){ minA[i] = Probes[j][p]->Acceleration[i];}
        }
    }

    for (int p = 0; p < Np; p++) {
        for(int i=0; i<Nd; i++) {
            if(fabs(minA[i]) != fabs(maxA[i])){
                Probes[j][p]->Acceleration[i] =  (Probes[j][p]->Acceleration[i]-minA[i])/(maxA[i]-minA[i]);
            }
        }
    }
    // Scale & Perturb Accel == 0
    /*
    Primes primes(j);
    for (int p = 0; p < Np; p++) {
        for(int i=0; i<Nd; i++) {

            if(Probes[j][p]->Acceleration[i] == 0 || Probes[j][p]->Acceleration[i] == 1){
                
                if(Utils::corputBase(primes.nextPrime(), p*Nd+i) < 0.5){
                    Probes[j][p]->Acceleration[i] += Utils::corputBase(primes.nextPrime(), p*Nd+i+1);
                }else{
                    Probes[j][p]->Acceleration[i] += 1-Utils::corputBase(primes.nextPrime(), p*Nd+i+1);
                }
            }else if(fabs(minA[i]) != fabs(maxA[i])){
                
                Probes[j][p]->Acceleration[i] =  (Probes[j][p]->Acceleration[i]-minA[i])/(maxA[i]-minA[i]);
            }
        }
    }
    */
    timer.stopTimer();
    accelTime += timer.getElapsedTime();
    maxA.clear(); minA.clear();
}

bool CFO_Binary::hasConverged(int j){
    bool converged = false;
    timer.startTimer();
    lastStep = j;
    
    if(hasFitnessSaturated(25, j) || j >= Nt ) {converged = true;}

    timer.stopTimer();
    convergeTime += timer.getElapsedTime();
    
    return converged;
}

double CFO_Binary::run(){
    timer1.startTimer();
            
    reset(); // Make sure everything is at original settings
    
    initA();
    IPD();
    evalFitness(0);

    lastStep = Nt;
    for (int j = 1; j < Nt; j++){
        
        updatePositions(j);
        evalFitness(j);			
        updateAcceleration(j);
        if(useAccelClipping){clipAcceleration(j);}
        if(hasConverged(j)){
                lastStep = j;
            break;
        }

        Frep += deltaFrep;
        if(Frep > 1){ Frep = 0.05; }

        //calculateDiversity(j);
        // std::cout << bestFitness << std::endl;
    }
    //End Time Steps
    timer1.stopTimer();
    totalTime += timer1.getElapsedTime();
    
    return bestFitness;
}