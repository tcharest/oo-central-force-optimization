/* This file is part of CFO in C++.

    CFO in C++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CFO in C++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CFO in C++.  If not, see <http://www.gnu.org/licenses/>.
    
    Copyright 2009 - 2012 Robert C. Green II - Robert.C.Green@gmail.com
*/

#include "CFO_Extended.h"

CFO_Extended::CFO_Extended(){
    cfoType = LINEAR;
}

CFO_Extended::CFO_Extended(	int _Np, int _Nd, int _Nt,
            double _Alpha, double _Beta, double _Gamma,
            double _Frep, double _deltaFrep, std::vector<double> min, std::vector<double> max,
            double(*_objFunc)(std::vector <std::vector<std::shared_ptr<Probe>>>& ,int, int, int),
            std::string _functionName,
            IPD_TYPE _ipdType,
            bool _useAccelClipping, double _aMax){

    init(_Np, _Nd, _Nt, _Alpha, _Beta, _Gamma, _Frep, _deltaFrep, min, max, _objFunc, _functionName, _ipdType, _useAccelClipping, _aMax);
    cfoType = EXTENDED;
    St = 0;
}

CFO_Extended::CFO_Extended(	int _Np, int _Nd, int _Nt,
            double _Alpha, double _Beta, double _Gamma,
            double _Frep, double _deltaFrep, double min, double max,
            double(*_objFunc)(std::vector <std::vector<std::shared_ptr<Probe>>>& ,int, int, int),
            std::string _functionName,
            IPD_TYPE _ipdType,
            bool _useAccelClipping , double _aMax) {

    std::vector < double > _xMin(_Np, min);
    std::vector < double > _xMax(_Np, max);
    init(_Np, _Nd, _Nt, _Alpha, _Beta, _Gamma, _Frep, _deltaFrep, _xMin, _xMax, _objFunc, _functionName, _ipdType, _useAccelClipping, _aMax);
    
    cfoType	= EXTENDED;
    St = 0;
}
CFO_Extended::~CFO_Extended(){}

double CFO_Extended::unitStep(double X) {
    double retValue;

    if (X >= -St){ retValue = 1.0;}
    else         { retValue = 0.0;}
    
    return retValue;
}

void CFO_Extended::updatePositions(int j){
    timer.startTimer();
    for (int p = 0; p < Np; p++) {
        for (int i = 0; i < Nd; i++) {

            Probes[j][p]->Position[i] = Probes[j - 1][p]->Position[i] + Probes[j - 1][p]->Acceleration[i];
            if(j > 1){ 
                Probes[j][p]->Position[i] += Probes[j - 2][p]->Acceleration[i]; 
            }
        }
    }
    timer.stopTimer();
    positionTime += timer.getElapsedTime();
}

void CFO_Extended::updateAcceleration(int j){
    timer.startTimer();

    for (int p = 0; p < Np; p++) {
        for (int i = 0; i < Nd; i++) {
            Probes[j][p]->Acceleration[i] = 0;

            /*********** Modification for Extended CFO ***********/
            St = 0.0;
            for (int k = 0; k < Np; k++) {
                if (k != p) {
                    St += Probes[j][k]->Fitness - Probes[j][p]->Fitness;
                }
            }
            St /= (Np-1);
            /****************************************************/

            for (int k = 0; k < Np; k++) {
                if (k != p) {
                    calculateInfluence(p, i, k, j);
                }
            }
        }
    }
    timer.stopTimer();
    accelTime += timer.getElapsedTime();
}
