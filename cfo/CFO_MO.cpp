/* This file is part of CFO_MO in C++.

    CFO_MO in C++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CFO_MO in C++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CFO_MO in C++.  If not, see <http://www.gnu.org/licenses/>.
    
    Copyright 2009 - 2012 Robert C. Green II - Robert.C.Green@gmail.com
*/

#include "CFO_MO.h"
CFO_MO::CFO_MO(){
    cfoType = STANDARD;
}

CFO_MO::CFO_MO( int _Np, int _Nd, int _Nt,
            double _Alpha, double _Beta, double _Gamma,
            double _Frep, double _deltaFrep, std::vector<double> min, std::vector<double> max,
            std::vector<double>(*_objFunc)(std::vector <std::vector<std::shared_ptr<Probe>>>& ,int, int, int),
            std::string _functionName,
            IPD_TYPE _ipdType,
            bool _useAccelClipping, double _aMax){
    ipdType = UNIFORM_ON_AXIS;
    init(_Np, _Nd, _Nt, _Alpha, _Beta, _Gamma, _Frep, _deltaFrep, min, max, _objFunc, _functionName, _ipdType, _useAccelClipping, _aMax);
}

CFO_MO::CFO_MO( int _Np, int _Nd, int _Nt,
            double _Alpha, double _Beta, double _Gamma,
            double _Frep, double _deltaFrep, double min, double max,
            std::vector<double>(*_objFunc)(std::vector <std::vector<std::shared_ptr<Probe>>>& ,int, int, int),
            std::string _functionName,
            IPD_TYPE _ipdType,
            bool _useAccelClipping, double _aMax) {
        
    std::vector < double > _xMin(_Np, min);
    std::vector < double > _xMax(_Np, max);
    ipdType = UNIFORM_ON_AXIS;
    init(_Np, _Nd, _Nt, _Alpha, _Beta, _Gamma, _Frep, _deltaFrep, _xMin, _xMax, _objFunc, _functionName, _ipdType, _useAccelClipping, _aMax);
}

CFO_MO::~CFO_MO() {
    xMin.clear(); xMax.clear();
	M.clear();
    bestProbeNumberArray.clear();
    bestFitnessArray.clear();
    bestFitness.clear(); worstFitness.clear();
    diversity.clear();
    objFunc = NULL;
}

void CFO_MO::init(int _Np, int _Nd, int _Nt,
                double _Alpha, double _Beta, double _Gamma,
                double _Frep, double _deltaFrep, std::vector<double> _xMin, std::vector<double> _xMax,
                std::vector<double>(*_objFunc)(std::vector <std::vector<std::shared_ptr<Probe>>>& ,int, int, int),
                std::string _functionName,
                IPD_TYPE _ipdType,
                bool _useAccelClipping, double _aMax){
                
    Np      = _Np;    Nd  = _Nd;    Nt = _Nt;
    Alpha   = _Alpha; Beta = _Beta; Gamma = _Gamma;
    
    Frep    = _Frep;    deltaFrep    = _deltaFrep;
    xMin    = _xMin;    xMax         = _xMax;
    objFunc = _objFunc; functionName = _functionName;
    
    originalXMin = _xMin; originalXMax = _xMax;

    useAccelClipping = _useAccelClipping; aMax = _aMax;

    ipdType = _ipdType;
    cfoType = MOCFO;
}

void CFO_MO::evalFitness(int j){
    timer.startTimer();
    for (int p = 0; p < Np; p++) {
        //Probes[j][p]->Fitness = objFunc(Probes, Nd, p, j);
        //numEvals++;
        //
        //if(Probes[j][p]->Fitness >= bestFitness){
        //    bestFitness     = Probes[j][p]->Fitness;
        //    bestTimeStep    = j;
        //    bestProbeNumber = p;
        //    
        //    bestFitnessArray[j]     = Probes[j][p]->Fitness;
        //    bestProbeNumberArray[j] = p;
        //}
        //if(Probes[j][p]->Fitness < worstFitness){
        //    worstFitness    = Probes[j][p]->Fitness;
        //    worstTimeStep   = j;
        //    worstProbeNumber = p;
        //}

    }
    timer.stopTimer();
    fitnessTime += timer.getElapsedTime();
}

void CFO_MO::updateAcceleration(int j){
    timer.startTimer();
    for (int p = 0; p < Np; p++) {
        for (int i = 0; i < Nd; i++) {
            Probes[j][p]->Acceleration[i] = 0;

            for (int k = 0; k < Np; k++) {
                if (k != p) {
                    SumSQ = 0.0;

                    for (int L = 0; L < Nd; L++) {  
                        SumSQ = SumSQ + pow(Probes[j][k]->Position[L] - Probes[j][p]->Position[L], 2);
                    }
                    if(SumSQ != 0){
                        Denom = sqrt(SumSQ);
                        
                        Numerator  = unitStep((M[k][j][0] - M[p][j][0])) * (M[k][j][0]- M[p][j][0]);
                        Numerator += unitStep((M[k][j][1] - M[p][j][1])) * (M[k][j][1]- M[p][j][1]);
                        
                        Probes[j][p]->Acceleration[i] = Probes[j][p]->Acceleration[i] + (Probes[j][k]->Position[i] - Probes[j][p]->Position[i]) * pow(Numerator,Alpha)/pow(Denom,Beta);
                    }
                }
            }
        }
    }
    timer.stopTimer();
    accelTime += timer.getElapsedTime();
}


void CFO_MO::reset(){

    bestTimeStep     = 0;  worstTimeStep    = 0;
    bestProbeNumber  = 0;  worstProbeNumber = 0;
    numProbesPerAxis = Np/Nd;
    if(numProbesPerAxis == 0){ numProbesPerAxis = 1;}

    numEvals         = 0;
    numCorrections   = 0;
    
#ifdef _WIN32
    bestFitness.resize(2, -std::numeric_limits<double>::infinity());
    worstFitness.resize(2, -std::numeric_limits<double>::infinity());
    M.resize(Np, std::vector < std::vector< double > >(Nt, std::vector < double >(2, -std::numeric_limits<double>::infinity())));
#else
    bestFitness.resize(2, -INFINITY);
    worstFitness.resize(2, INFINITY);
    M.resize(Np, std::vector < std::vector< double > >(Nt, std::vector < double >(2, -INFINITY)));
#endif
    positionTime = 0, correctionTime = 0, 
    fitnessTime  = 0, accelTime      = 0, 
    shrinkTime   = 0, convergeTime   = 0,
    totalTime    = 0;
    
	M.clear();
    bestProbeNumberArray.clear();
    bestFitnessArray.clear();
    diversity.clear();
    
	//Probes.resize(Nt, std::vector < std::shared_ptr<Probe> >(Np, std::shared_ptr<Probe>(new Probe(Nd)) ));
	Probes.resize(Nt);
	for (auto& i : Probes) {
		i.reserve(Np);
		for (int j = 0; Np != j; ++j) {
			i.push_back(std::make_unique<Probe>(Nd));
		}
	}
    bestProbeNumberArray.resize(Nt,0);
    bestFitnessArray.resize(Nt, std::vector<double>(2, 0));
    
    xMin = originalXMin;
    xMax = originalXMax;
}


double CFO_MO::run(){
    timer1.startTimer();
            
    reset(); // Make sure everything is at original settings
    
    IPD();
    initA();    
    evalFitness(0);

    lastStep = Nt;
    for (int j = 1; j < Nt; j++){
        
        updatePositions(j);
        correctErrors(j);
        evalFitness(j);
        updateAcceleration(j);

        if(useAccelClipping){
            clipAcceleration(j);
        }
    
        // Adjust Frep
        Frep += deltaFrep;
        if(Frep > 1) Frep = 0.05;

        adjustBoundaries(j);

        if(hasConverged(j)){
            lastStep = j;
            break;
        }
        calculateDiversity(j);
    }//End Time Steps
    timer1.stopTimer();
    totalTime += timer1.getElapsedTime();
    
    cleanUp();

    //return bestFitness;
    return 0.0;
}

bool CFO_MO::hasFitnessSaturated(int nSteps, int j) {

    double fitnessSatTol    = 0.000001;
    #ifdef _WIN32
        double bestFitness = -std::numeric_limits<double>::infinity();
    #else
        double bestFitness  = -INFINITY;
    #endif
    #ifdef _WIN32
        double bestFitnessStepJ = -std::numeric_limits<double>::infinity();
    #else
        double bestFitnessStepJ  = -INFINITY;
    #endif
    double sumOfBestFitness = 0;
    bool retValue = false;

    if (j < nSteps + 10){
        retValue = false;
    }else{
        for (int k = j - nSteps + 1; k <= j; k++) {

            #ifdef _WIN32
                bestFitness = -std::numeric_limits<double>::infinity();
            #else
                bestFitness = -INFINITY;
            #endif
            for (int p = 0; p < Np; p++) {
                if (M[p][k][0] >= bestFitness) {
                    bestFitness = M[p][k][0];
                }
            }
            if(k == j){
                bestFitnessStepJ = bestFitness;
            }
            sumOfBestFitness += bestFitness;
        }
        if (fabs(sumOfBestFitness / nSteps - bestFitnessStepJ) <= fitnessSatTol) {
            retValue = true;
        }
    }
    return retValue;
}

std::vector < std::vector < double > > CFO_MO::getBestFitnessArray()        { return bestFitnessArray; }


void CFO_MO::setObjectiveFunction(std::vector<double>(*of)(std::vector <std::vector<std::shared_ptr<Probe>>>& ,int, int, int)){ objFunc = of; }

void CFO_MO::printResults(){
    std::cout << std::setw(23) << std::left << "Function Name:"    << functionName << std::endl;
    std::cout << std::setw(23) << std::left << "Best Fitness 1:"   << std::setprecision(10) << bestFitness[0] << std::endl;
    std::cout << std::setw(23) << std::left << "Best Fitness 2:"   << std::setprecision(10) << bestFitness[1] << std::endl;
    std::cout << std::setw(23) << std::left << "Number of Probes:" << Np << std::endl;
    std::cout << std::setw(23) << std::left << "Probes Per Axis:"  << numProbesPerAxis << std::endl;
    std::cout << std::setw(23) << std::left << "Best Probe #:"     << std::setprecision(0) << bestProbeNumber << std::endl;
    std::cout << std::setw(23) << std::left << "Best Gamma:"       << std::setprecision(2) << Gamma << std::endl;
    std::cout << std::setw(23) << std::left << "Best Time Step:"   << bestTimeStep << std::endl;
    std::cout << std::setw(23) << std::left << "Last Step:"        << std::setprecision(0) << lastStep << std::endl;
    std::cout << std::setw(23) << std::left << "Evaluations:"      << std::setprecision(0) << numEvals << std::endl;
    std::cout << std::setw(23) << std::left << "Corrections:"       << std::setprecision(0) << numCorrections << std::endl;

    std::cout << "-------------------------- Times --------------------------"           << std::endl;
    std::cout << std::setw(23) << std::left << "Position:"      << std::setprecision(4) << positionTime      << std::endl;
    std::cout << std::setw(23) << std::left << "Correction:"    << std::setprecision(4) << correctionTime << std::endl;
    std::cout << std::setw(23) << std::left << "Fitness:"       << std::setprecision(4) << fitnessTime   << std::endl;
    std::cout << std::setw(23) << std::left << "Acceleration:"  << std::setprecision(4) << accelTime     << std::endl;
    std::cout << std::setw(23) << std::left << "Clipping:"      << std::setprecision(4) << clippingTime      << std::endl;
    std::cout << std::setw(23) << std::left << "Shrink:"        << std::setprecision(4) << shrinkTime    << std::endl;
    std::cout << std::setw(23) << std::left << "Converge:"      << std::setprecision(4) << convergeTime      << std::endl;
    std::cout << std::endl;
}

void CFO_MO::printDelimitedResults(std::string del){
    std::cout   << functionName                     << del
                << Utils::cfoTypeString(cfoType)    << del
                << Utils::ipdString(ipdType)        << del
                << bestFitness[0]                   << del
                << bestFitness[1]                   << del
                << bestProbeNumber                  << del
                << bestTimeStep                     << del
                << Gamma                            << del
                << numProbesPerAxis                 << del
                << Np                               << del
                << Nd                               << del
                << lastStep                         << del
                << numEvals                         << del
                << numCorrections                   << del
                << positionTime                     << del
                << correctionTime                   << del
                << fitnessTime                      << del
                << accelTime                        << del
                << shrinkTime                       << del
                << convergeTime                     << del
                << totalTime                        << std::endl;

    if(logTofile){
        std::ofstream myFile;
        std::string fileName;
        
        fileName = "CFO_MO_" + functionName;
        fileName += "_" + Utils::cfoTypeString(cfoType) + "_";
        fileName += "_" + Utils::ipdString(ipdType) + "_";
        fileName = Utils::changeBase(fileName, Nd);
        fileName = fileName + ".csv";
        myFile.open(fileName.c_str(), std::ios::app);
        if(myFile.is_open()){
            myFile  << functionName                     << del
                    << Utils::cfoTypeString(cfoType)    << del
                    << Utils::ipdString(ipdType)        << del
                    << bestFitness[0]                   << del
                    << bestFitness[1]                   << del
                    << bestProbeNumber                  << del
                    << bestTimeStep                     << del
                    << Gamma                            << del
                    << numProbesPerAxis << del
                    << Np                               << del
                    << Nd                               << del
                    << lastStep                         << del
                    << numEvals                         << del
                    << numCorrections                   << del
                    << positionTime                     << del
                    << correctionTime                   << del
                    << fitnessTime                      << del
                    << accelTime                        << del
                    << shrinkTime                       << del
                    << convergeTime                     << del
                    << totalTime                        << std::endl;
        }
        myFile.close();

        fileName = "CFO_MO_Conv_" + functionName;
        fileName += "_" + Utils::cfoTypeString(cfoType) + "_";
        fileName += "_" + Utils::ipdString(ipdType) + "_";
        fileName = Utils::changeBase(fileName, Np);
        fileName += "_"; 
        fileName = Utils::changeBase(fileName, Nd);
        fileName = fileName + ".csv";
        myFile.open(fileName.c_str(), std::ios::trunc);
        if(myFile.is_open()){
            for(int i=0; i<lastStep; i++){
                myFile << i << " " << bestFitnessArray[i][0] << " " << bestFitnessArray[i][1] << std::endl;
            }
        }
        myFile.close();

        fileName = "CFO_MO_Probe_Conv_Pos_" + functionName + "_";
        fileName += Utils::cfoTypeString(cfoType) + "_";
        fileName += Utils::ipdString(ipdType) + "_";
        fileName = Utils::changeBase(fileName, Np);
        fileName += "_"; 
        fileName = Utils::changeBase(fileName, Nd);
        fileName = fileName + ".csv";
        myFile.open(fileName.c_str(), std::ios::trunc);
        if(myFile.is_open()){
            for(int j=0; j<lastStep; j++){
                for(int p=0; p<Np; p++){
                    for(int i=0; i<Nd; i++){
                        myFile << Probes[j][p]->Position[i] << " ";
                    }
                }
                myFile << std::endl;
            }
        }
        myFile.close();

        fileName = "CFO_MO_Probe_Conv_Fitness_" + functionName + "_";
        fileName += Utils::cfoTypeString(cfoType) + "_";
        fileName += Utils::ipdString(ipdType) + "_";
        fileName = Utils::changeBase(fileName, Np);
        fileName += "_"; 
        fileName = Utils::changeBase(fileName, Nd);
        fileName = fileName + ".csv";
        myFile.open(fileName.c_str(), std::ios::trunc);
        if(myFile.is_open()){
            for(int j=0; j<lastStep; j++){
                myFile << bestProbeNumberArray[j] << " ";
                for(int p=0; p<Np; p++){
                    myFile << M[p][j][0] << " " << M[p][j][1];
                }
                myFile << std::endl;
            }
        }
        myFile.close();

        fileName = "CFO_MO_Diversity_" + functionName;
        fileName += "_" + Utils::cfoTypeString(cfoType) + "_";
        fileName += "_" + Utils::ipdString(ipdType) + "_";
        fileName = Utils::changeBase(fileName, Np);
        fileName = fileName + "_"; 
        fileName = Utils::changeBase(fileName, Nd);
        fileName = fileName + ".csv";
        myFile.open(fileName.c_str(), std::ios::trunc);
        if(myFile.is_open()){
            for(unsigned int i=0; i<diversity.size(); i++){
                myFile << i << " " << diversity[i] << std::endl;
            }
        }
        myFile.close();
    }
}
