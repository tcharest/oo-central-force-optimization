/* This file is part of CFO in C++.

    CFO in C++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CFO in C++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CFO in C++.  If not, see <http://www.gnu.org/licenses/>.
    
    Copyright 2009 - 2012 Robert C. Green II - Robert.C.Green@gmail.com
*/

#ifndef CFO_H_
#define CFO_H_

#include <algorithm>
#include <cmath>
#include <iostream>
#include <iomanip>
#include <vector>
#include <string>
#include <omp.h>
#include <memory>

#include "CStopWatch.h"
#include "defs.h"
#include "MTRand.h"
#include "Utils.h"
#include "Probe.h"

#ifdef _WIN32
    #include <limits>
#endif

class CFO {

    public:
        CFO();
        CFO(int _Np, int _Nd, int _Nt,
                double _Alpha, double _Beta, double _Gamma,
                double _Frep, double _deltaFrep, std::vector<double> min, std::vector<double> max,
                double(*_objFunc)(std::vector <std::vector<std::shared_ptr<Probe>>>& ,int, int, int),
                std::string _functionName,
                IPD_TYPE _ipdType = UNIFORM_ON_AXIS,
                bool _useAccelClipping = false, double _aMax = 1);
        CFO(int _Np, int _Nd, int _Nt,
                double _Alpha, double _Beta, double _Gamma,
                double _Frep, double _deltaFrep, double min, double max,
                double(*_objFunc)(std::vector <std::vector<std::shared_ptr<Probe>>>& ,int, int, int),
                std::string _functionName,
                IPD_TYPE _ipdType = UNIFORM_ON_AXIS,
                bool _useAccelClipping = false, double _aMax = 1);
        virtual ~CFO();
        
        int getBestTimeStep();
        int getBestProbeNumber();
        int getLastStep();
        int getNumEvals();
        int getNumCorrections();
        
        virtual double run();
        double getBestFitness();
        double getPositiontime();
        double getCorrectionTime();
        double getFitnessTime();
        double getAccelTime();
        double getShrinkTime();
        double getConvergeTime();
        
        std::vector <int> getBestProbeNumberArray();
        std::vector <double> getBestFitnessArray();
        
        void setNp(int n);
        void setNd(int n);
        void setNt(int n);
        void setAlpha(double a);
        void setBeta(double b);
        void setGamma(double g);
        void setFrep(double fr);
        void setDeltaFrep(double dfr);
        void setThreads(int thr);
        void setXMin(double min);
        void setXMax(double max);
        void setXMin(std::vector<double> min);
        void setXMax(std::vector<double> max);
        void setFunctionName(std::string fname);
        void setObjectiveFunction(double(*of)(std::vector <std::vector<std::shared_ptr<Probe>>>& ,int, int, int));
        void setAMax(double _aMax);
        void setIPDType(IPD_TYPE _ipdType);
        void setUseAccelClipping(bool _useAccelClipping);
        void setLogToFile(bool ltf);
                
        void printResults();
        void recordValues();
        virtual void printDelimitedResults(std::string del = " ");
        virtual void printDelimitedResultsToFile(std::string del = " ");


    protected:
        virtual void updateAcceleration(int j);
        virtual void calculateInfluence(int p, int i, int k, int j);

        virtual bool hasConverged(int j);
        virtual bool hasFitnessSaturated(int nSteps, int j);
        void init(int _Np, int _Nd, int _Nt,
                double _Alpha, double _Beta, double _Gamma,
                double _Frep, double _deltaFrep, std::vector<double> _xMin, std::vector<double> _xMax,
                double(*_objFunc)(std::vector <std::vector<std::shared_ptr<Probe>>>& ,int, int, int),
                std::string _functionName,
                IPD_TYPE _ipdType,
                bool _useAccelClipping, double _aMax);


        void adjustBoundaries(int j);
        void clipAcceleration(int j);
        virtual void correctErrors(int j);
        virtual void evalFitness(int j);
        virtual void initA();
        virtual void cleanUp();

        virtual void updatePositions(int j);
        virtual void reset();

        void IPD();
        virtual void UniformOnDiagonal();
        virtual void UniformOnAxis();
        virtual void Random();
        virtual void Random_VDC();
        virtual void latinHypercube();
        virtual void descSampling();
        virtual void ldsHalton();
        virtual void ldsHammersley();
        virtual void ldsFaure();

        virtual void ldsHalton_Transpose();
        virtual void ldsHammersley_Transpose();
        virtual void ldsFaure_Transpose();

        virtual void calculateDiversity(int j);

        //double corputBase(double base, double number);
        void   calculateDiagLength();
        double unitStep(double X);
        bool   useAccelClipping, logTofile;

        IPD_TYPE ipdType;
        CFO_TYPE cfoType;
        int Np, Nd, Nt, numProbesPerAxis, lastStep;
        int bestTimeStep, bestProbeNumber;
        int worstTimeStep, worstProbeNumber;
        int numEvals, numCorrections, threads;

        double Alpha, Beta, Gamma;
        double Frep, deltaFrep;
        double bestFitness, diagLength;
        double worstFitness;
        double Denom, Numerator;
        double SumSQ, DiagLength;
        double aMax;
        double positionTime, correctionTime, 
                    fitnessTime,  accelTime, 
                    shrinkTime,   convergeTime,
                    totalTime, 	  clippingTime;

        double(*objFunc)(std::vector <std::vector<std::shared_ptr<Probe>>> & ,int, int, int);

        std::vector <double> originalXMin, originalXMax;
        std::vector <double> xMin, xMax;
		std::vector <std::vector<std::shared_ptr<Probe>>> Probes;
        std::vector < double > bestFitnessArray;
        std::vector < int >  bestProbeNumberArray;
        std::vector < double> diversity;

        std::string functionName;
        CStopWatch timer, timer1;
};

#endif /* CFO_H_ */
