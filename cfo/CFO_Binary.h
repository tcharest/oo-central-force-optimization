/* This file is part of Binary_CFO in C++.

    Binary_CFO in C++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Binary_CFO in C++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Binary_CFO in C++.  If not, see <http://www.gnu.org/licenses/>.
    
    Copyright 2009 - 2012 Robert C. Green II - Robert.C.Green@gmail.com
*/

#ifndef BINARY_CFO_H_
#define BINARY_CFO_H_

#include "CFO.h"

class CFO_Binary : public CFO {

    public:
        CFO_Binary();
        CFO_Binary(int _Np, int _Nd, int _Nt,
                double _Alpha, double _Beta, double _Gamma,
                double _Frep, double _deltaFrep, 
                double(*_objFunc)(std::vector <std::vector<std::shared_ptr<Probe>>>& ,int, int, int),
                std::string _functionName,
                IPD_TYPE _ipdType = UNIFORM_ON_AXIS,
                bool _useAccelClipping = false, double _aMax = 1);

        virtual double run();

    protected:
        
        virtual void calculateDiversity(int j);
        virtual bool hasConverged(int j);
        virtual void updateAcceleration(int j);
        virtual void updatePositions(int j);
        virtual void initA();

        void IPD();
        void init(int _Np, int _Nd, int _Nt,
                double _Alpha, double _Beta, double _Gamma,
                double _Frep, double _deltaFrep,
                double(*_objFunc)(std::vector <std::vector<std::shared_ptr<Probe>>>& ,int, int, int),
                std::string _functionName,
                IPD_TYPE _ipdType,
                bool _useAccelClipping, double _aMax);

        virtual void evalFitness(int j);
        virtual void reset();

        double sigmoid(double x);
};

#endif /* Binary_CFO_H_ */
