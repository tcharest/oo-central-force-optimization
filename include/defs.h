#ifndef DEFS_H_
#define DEFS_H_

#include <vector>

enum CFO_TYPE { STANDARD, RING, STAR, MESH, STATIC_TREE, LINEAR, TOROIDAL, BINARY, CUDA, 
                GLOBAL_BEST, GLOBAL_BESTWORST, MOCFO, EXTENDED, MULTIPLICITY};

enum IPD_TYPE {	UNIFORM_ON_DIAGONAL	=0,     RANDOM				=1, RANDOM_VDC		=2,	
				LATIN_HYPERCUBE		=3,		DESC_SAMP			=4,  
				LDS_HALTON			=5,		LDS_HAMMERSLEY		=6,	LDS_FAURE		=7, 
				LDS_HALTON_TRANS	=8,		LDS_HAMMERSLEY_TRANS=9, LDS_FAURE_TRANS	=10, 
				UNIFORM_ON_AXIS		=11};

typedef std::vector<int>         iArray1D;
typedef std::vector<iArray1D>    iArray2D;
typedef std::vector<iArray2D>    iArray3D;

typedef std::vector<double>      ldArray1D;
typedef std::vector<ldArray1D>   ldArray2D;
typedef std::vector<ldArray2D>   ldArray3D;

#endif