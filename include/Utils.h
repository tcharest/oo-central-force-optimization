/* This file is part of CFO in C++.

    CFO in C++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CFO in C++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
	along with CFO in C++.  If not, see <http://www.gnu.org/licenses/>.
	
	Copyright 2009 - 2012 Robert C. Green II - Robert.C.Green@gmail.com
*/

#ifndef UTILS_H_
#define UTILS_H_

#include <algorithm>
#include <fstream>
#include <iomanip>
#include <iterator>
#include <math.h>
#include <sstream>
#include <stdio.h>
#include <string>
#include <vector>
#include <iostream>
#include <sqlite3.h> 


#ifndef _OPENMP
	#include "omp.h"
#endif

#include "anyoption.h"
#include "defs.h"
#include "MTRand.h"
#include "Primes.h"
#include "fftw3.h"

namespace Utils {

    /************************ Database Logging ************************/
 	extern int callback(void *NotUsed, int argc, char **argv, char **azColName);
    extern void setupDb();
    extern void recordValues(std::string functionName, double fitness, int bestProbe, int bestTimeStep, double Gamma, 
                             int NpNd, int Np, int Nd, int lastStep, int Evals, int Corrections, double posTime, 
                             double cortime, double fitTime, double accelTime, double shrinkTime, double convTime, 
                             double totalTime, int threads, CFO_TYPE cfoType, IPD_TYPE ipdType);
    extern int fft2d(std::vector < std::vector < double > > , int nx, int ny, int dir);

	extern std::vector<double> decToBin(double n, int numDigits);
	extern std::vector< std::vector<int> > matMult(std::vector< std::vector<int> > A, std::vector< std::vector<int> > B);
	extern std::vector < std::vector < double > > faureSampling(int Nd, int Ns);
	extern std::vector<int> changeBase(double num, double base, int numDigits);
	extern double corputBase(double base, double number);

	extern std::vector < std::vector < double > > latinHyperCube_Random(int numVars, int numSamples, MTRand& mt);
	extern std::vector < std::vector < double > > descriptiveSampling_Random(int numVars, int numSamples, MTRand& mt);
	extern std::vector < std::vector < double > > descriptiveSampling_Omp(int numVars, int numSamples, MTRand& mt);
	extern std::vector < std::vector < double > > hammersleySampling(int Nd, int Ns);
	extern std::vector < std::vector < double > > haltonSampling(int Nd, int Ns);
	extern std::vector < std::vector < double > > faureSampling(int Nd, int Ns);

	extern std::string toLower(std::string str);
	extern std::string toUpper(std::string str);
	extern std::vector<std::string> permuteCharacters(std::string topermute);
	extern std::string changeBase(std::string Base, int number);

	extern std::string vectorToString(std::vector<double> v);
	extern std::string vectorToString(std::vector<int> v);
	extern std::string arrayToString(int* v, int size);

	extern std::string ipdString(IPD_TYPE ipdType);
	extern std::string cfoTypeString(CFO_TYPE ct);

	extern double sigMoid(double v);
	extern void tokenizeString(std::string str,std::vector<std::string>& tokens,const std::string& delimiter );
	extern int factorial(int n);
	extern int combination(int n, int r);  		
    		
    extern char* getTimeStamp();
    extern void twoByTwoCholeskyDecomp(std::vector<std::vector<double> >& A);

    extern void setOptions(AnyOption* opt);

};

#endif /* UTILS_H_ */
